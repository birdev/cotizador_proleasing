<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Bien
 * @package App\Models
 * @version November 9, 2018, 6:06 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Cotizacione
 * @property string nombre
 * @property string|\Carbon\Carbon update_at
 * @property string|\Carbon\Carbon delated_at
 */
class Bien extends Model
{
    use SoftDeletes;

    public $table = 'bienes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cotizaciones()
    {
        return $this->hasMany(\App\Models\Cotizacione::class);
    }
}
