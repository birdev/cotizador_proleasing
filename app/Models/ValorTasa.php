<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ValorTasa
 * @package App\Models
 * @version November 9, 2018, 6:06 pm UTC
 *
 * @property \App\Models\Tasa tasa
 * @property \Illuminate\Database\Eloquent\Collection cotizaciones
 * @property integer tasa_id
 * @property decimal valor
 */
class ValorTasa extends Model
{
    use SoftDeletes;

    public $table = 'valoresTasas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tasa_id',
        'valor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tasa_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tasa()
    {
        return $this->belongsTo(\App\Models\Tasa::class);
    }
}
