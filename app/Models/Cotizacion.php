<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cotizacion
 * @package App\Models
 * @version November 9, 2018, 6:07 pm UTC
 *
 * @property \App\Models\Biene biene
 * @property \App\Models\User user
 * @property integer user_id
 * @property string uri_pdf
 * @property string nombre_cliente
 * @property string apellido_paterno_cliente
 * @property string apellido_materno_cliente
 * @property string empresa_cliente
 * @property string telefono_cliente
 * @property string email_cliente
 * @property integer bien_id
 * @property float valor_factura_sin_iva
 * @property float porcentaje_comision
 * @property integer plazo
 * @property float porcentaje_pago_inicial
 * @property float valor_residual_calculo
 * @property float porcentaje_adicional_vr_impreso
 * @property float porcentaje_tasa
 * @property float pago_inicial
 * @property float subtotal
 * @property float pago_mensual_fijo
 * @property float iva
 * @property float renta_en_deposito
 * @property float total_pago_inicial
 * @property float valor_residual_sin_iva
 */
class Cotizacion extends Model
{
    use SoftDeletes;

    public $table = 'cotizaciones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'uri_pdf',
        'nombre_cliente',
        'apellido_paterno_cliente',
        'apellido_materno_cliente',
        'empresa_cliente',
        'telefono_cliente',
        'email_cliente',
        'bien_id',
        'marca',
        'modelo',
        'anio',
        'valor_factura_sin_iva',
        'valor_factura_con_iva',
        'porcentaje_comision',
        'plazo',
        'porcentaje_pago_inicial',
        'valor_residual_calculo',
        'porcentaje_adicional_vr_impreso',
        'porcentaje_tasa',
        'pago_inicial',
        'subtotal',
        'pago_mensual_fijo',
        'iva',
        'renta_en_deposito',
        'total_pago_inicial',
        'valor_residual_sin_iva',
        'localizador',
        'seguro',
        'valor_seguro',
        'gastos_de_apertura'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'uri_pdf' => 'string',
        'nombre_cliente' => 'string',
        'apellido_paterno_cliente' => 'string',
        'apellido_materno_cliente' => 'string',
        'empresa_cliente' => 'string',
        'telefono_cliente' => 'string',
        'email_cliente' => 'string',
        'bien_id' => 'integer',
        'plazo' => 'integer',
        'marca' => 'string',
        'modelo' => 'string',
        'anio' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bien()
    {
        return $this->belongsTo(\App\Models\Bien::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
