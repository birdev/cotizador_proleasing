<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version November 9, 2018, 6:05 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Cotizacione
 * @property string name
 * @property string apellido_paterno
 * @property string apellido_materno
 * @property string email
 * @property string password
 * @property string tipo_user
 * @property string remember_token
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'apellido_paterno',
        'apellido_materno',
        'email',
        'password',
        'tipo_user',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'apellido_paterno' => 'string',
        'apellido_materno' => 'string',
        'email' => 'string',
        'password' => 'string',
        'tipo_user' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cotizaciones()
    {
        return $this->hasMany(\App\Models\Cotizacione::class);
    }
}
