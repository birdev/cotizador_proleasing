<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Response;
use PDF;
class FrontController extends Controller
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $valoresPagoInicialGeneral = \App\Models\ValorTasa::select('valor')->where('tasa_id','=', 1)->orderBy('valor','asc')->get();
        $valoresPagoInicialNoParticulares = \App\Models\ValorTasa::where('tasa_id','=', 1)->where('bien_section','!=','Automóviles')->get();
        $valoresPlazo = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',2)->orderBy('valor','asc')->get();
        $comision = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',4)->first();
        $tasa = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',3)->first();
        
        $valorResidualAutos = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Automóviles')->get();
        $valorResidualFlotillas = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Flotillas')->get();
        $valorResidualCamiones = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Camiones')->get();
        $valorResidualMaquinaria = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Maquinaria')->get();
        $valorResidualElectronica = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Electrónica en general')->get();

        $valorResidualAdicional = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',6)->first();

        $localizador = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',7)->first();

        
        return view('front.index')
        ->with('valoresPagoInicialGeneral',$valoresPagoInicialGeneral)
        ->with('valoresPagoInicialNoParticulares',$valoresPagoInicialNoParticulares)
        ->with('valoresPlazo',$valoresPlazo)
        ->with('comision',$comision)
        ->with('tasa',$tasa)
        ->with('valorResidualAutos',$valorResidualAutos)
        ->with('valorResidualFlotillas',$valorResidualFlotillas)
        ->with('valorResidualCamiones',$valorResidualCamiones)
        ->with('valorResidualMaquinaria',$valorResidualMaquinaria)
        ->with('valorResidualElectronica',$valorResidualElectronica)
        ->with('valorResidualAdicional',$valorResidualAdicional)
        ->with('localizador',$localizador);
    }

     /**
     * Store a newly created Cotizacion in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function enviar_cotizacion(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
        $vfci  = str_replace(',','',$request->valor_factura_con_iva); 
        $vfci  = str_replace('$','',$vfci); 
        
        //Localizador
        if($request->formLocalizador != null)
        {
            $localizador = 'Si';
        }
        else{
            $localizador = 'No';
        }
        
        //Seguro
        if($request->formRadios == 'contado')
        {
            $seguro = 'contado';
            $valor_seguro = $request->poliza_seguro_anual;
        }
        else if($request->formRadios == 'financiado')
        {
            $seguro = 'financiado';
            $valor_seguro = $request->poliza_seguro_anual;
        }
        $marca = $request->marca;
        
        $bien = \App\Models\Bien::where('nombre','=',$request->bien)->first();
        $new_cotizacion =  \App\Models\Cotizacion::create([
        
        'user_id' => 7,
        'uri_pdf' => "enlace",
        'nombre_cliente' => $request->nombre_cliente,
        'apellido_paterno_cliente' => $request->apellido_paterno_cliente,
        'apellido_materno_cliente' => $request->apellido_materno_cliente,
        'empresa_cliente' => $request->empresa_cliente,
        'telefono_cliente' => $request->telefono_cliente,
        'email_cliente' => $request->email_cliente,
        'bien_id' => $bien->id,
        'marca' => $marca,
        'modelo' => $request->modelo,
        'anio' => intval($request->anio),
        'valor_factura_sin_iva' => floatval($request->valor_factura_sin_iva),
        'valor_factura_con_iva' => floatval($vfci),
        'porcentaje_comision' => $request->porcentaje_comision * 100,
        'plazo' => intval($request->plazo),
        'porcentaje_pago_inicial' => floatval($request->porcentaje_pago_inicial),
        'valor_residual_calculo' => floatval($request->valor_residual_calculo),
        'porcentaje_adicional_vr_impreso' => floatval($request->porcentaje_adicional_vr),
        'porcentaje_tasa' => floatval($request->porcentaje_tasa),
        'pago_inicial' => floatval($request->pago_inicial),
        'gastos_de_apertura' => floatval($request->gastos_de_apertura),
        'subtotal' => floatval($request->subtotal),
        'pago_mensual_fijo' => floatval($request->pago_mensual_fijo),
        'iva' => floatval($request->iva),
        'renta_en_deposito' => floatval($request->renta_en_deposito),
        'total_pago_inicial' => floatval($request->total_pago_inicial),
        'valor_residual_sin_iva' => floatval($request->valor_residual_sin_iva),
        'localizador' => $localizador,
        'seguro' => $seguro,
        'valor_seguro' => $valor_seguro
        ]);
        
        $cotizacion = \App\Models\Cotizacion::where('id','=',$new_cotizacion->id)->first();
        
        $pdf = PDF::loadView('cotizacions.pdf',['cotizacion' => $cotizacion]);
        
        return $pdf->stream();

        return redirect()->action('FrontController@index');
    }

    public function phpinfo()
    {
        return view('front.info');
    }
}