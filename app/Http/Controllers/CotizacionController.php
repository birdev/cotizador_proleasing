<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCotizacionRequest;
use App\Http\Requests\UpdateCotizacionRequest;
use App\Repositories\CotizacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use PDF;

class CotizacionController extends AppBaseController
{
    /** @var  CotizacionRepository */
    private $cotizacionRepository;

    public function __construct(CotizacionRepository $cotizacionRepo)
    {
        $this->cotizacionRepository = $cotizacionRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Cotizacion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $this->cotizacionRepository->pushCriteria(new RequestCriteria($request));
        // $cotizacions = $this->cotizacionRepository->all();
        $user = Auth::user();
        $cotizacions = \App\Models\Cotizacion::where('user_id','=',$user->id)->get();
        
        return view('cotizacions.index')
            ->with('cotizacions', $cotizacions);
    }

    /**
     * Show the form for creating a new Cotizacion.
     *
     * @return Response
     */
    public function create()
    {
        $valoresPagoInicialGeneral = \App\Models\ValorTasa::select('valor')->where('tasa_id','=', 1)->orderBy('valor','asc')->get();
        $valoresPagoInicialNoParticulares = \App\Models\ValorTasa::where('tasa_id','=', 1)->where('bien_section','!=','Automóviles')->get();
        $valoresPlazo = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',2)->orderBy('valor','asc')->get();
        $comision = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',4)->first();
        $tasa = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',3)->first();
        
        $valorResidualAutos = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Automóviles')->get();
        $valorResidualFlotillas = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Flotillas')->get();
        $valorResidualCamiones = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Camiones')->get();
        $valorResidualMaquinaria = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Maquinaria')->get();
        $valorResidualElectronica = \App\Models\ValorTasa::select('valor','plazo_vr')->where('tasa_id','=',5)->where('bien_section','=','Electrónica en general')->get();

        $valorResidualAdicional = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',6)->first();

        $localizador = \App\Models\ValorTasa::select('valor')->where('tasa_id','=',7)->first();

        return view('cotizacions.create')
        ->with('valoresPagoInicialGeneral',$valoresPagoInicialGeneral)
        ->with('valoresPagoInicialNoParticulares',$valoresPagoInicialNoParticulares)
        ->with('valoresPlazo',$valoresPlazo)
        ->with('comision',$comision)
        ->with('tasa',$tasa)
        ->with('valorResidualAutos',$valorResidualAutos)
        ->with('valorResidualFlotillas',$valorResidualFlotillas)
        ->with('valorResidualCamiones',$valorResidualCamiones)
        ->with('valorResidualMaquinaria',$valorResidualMaquinaria)
        ->with('valorResidualElectronica',$valorResidualElectronica)
        ->with('valorResidualAdicional',$valorResidualAdicional)
        ->with('localizador',$localizador);
        
    }

    /**
     * Store a newly created Cotizacion in storage.
     *
     * @param CreateCotizacionRequest $request
     *
     * @return Response
     */
    public function store(CreateCotizacionRequest $request)
    {        
        date_default_timezone_set('America/Mexico_City');
        $bien = \App\Models\Bien::where('nombre','=',$request->bien)->first();
        $new_cotizacion = new \App\Models\Cotizacion();
        $user = Auth::user();
        
        $new_cotizacion->user_id = $user->id;
        $new_cotizacion->uri_pdf = "enlace";
        $new_cotizacion->nombre_cliente = $request->nombre_cliente;
        $new_cotizacion->apellido_paterno_cliente = $request->apellido_paterno_cliente;
        $new_cotizacion->apellido_materno_cliente = $request->apellido_materno_cliente;
        $new_cotizacion->empresa_cliente = $request->empresa_cliente;
        $new_cotizacion->telefono_cliente = $request->telefono_cliente;
        $new_cotizacion->email_cliente = $request->email_cliente;
        $new_cotizacion->bien_id = $bien->id;
        $new_cotizacion->marca = $request->marca;
        $new_cotizacion->modelo = $request->modelo;
        $new_cotizacion->anio = intval($request->anio);
        $new_cotizacion->valor_factura_sin_iva = floatval($request->valor_factura_sin_iva);
        $new_cotizacion->valor_factura_con_iva = floatval($request->valor_factura_con_iva);
        $new_cotizacion->porcentaje_comision = $request->porcentaje_comision * 100;
        $new_cotizacion->plazo = intval($request->plazo);
        $new_cotizacion->porcentaje_pago_inicial = floatval($request->porcentaje_pago_inicial);
        $new_cotizacion->valor_residual_calculo = floatval($request->valor_residual_calculo);
        $new_cotizacion->porcentaje_adicional_vr_impreso = floatval($request->porcentaje_adicional_vr);
        $new_cotizacion->porcentaje_tasa = floatval($request->porcentaje_tasa);
        $new_cotizacion->pago_inicial = floatval($request->pago_inicial);
        $new_cotizacion->subtotal = floatval($request->subtotal);
        $new_cotizacion->pago_mensual_fijo = floatval($request->pago_mensual_fijo);
        $new_cotizacion->iva = floatval($request->iva);
        $new_cotizacion->renta_en_deposito = floatval($request->renta_en_deposito);
        $new_cotizacion->total_pago_inicial = floatval($request->total_pago_inicial);
        $new_cotizacion->valor_residual_sin_iva = floatval($request->valor_residual_sin_iva);
        $new_cotizacion->gastos_de_apertura = floatval($request->gastos_de_apertura);
        
        //Localizador
        if($request->formLocalizador != null)
        {
            $new_cotizacion->localizador = 'Si';
        }
        else{
            $new_cotizacion->localizador = 'No';
        }
        
        //Seguro
        if($request->formRadios == 'contado')
        {
            $new_cotizacion->seguro = 'contado';
            $new_cotizacion->valor_seguro = $request->poliza_seguro_anual;
        }
        else if($request->formRadios == 'financiado')
        {
            $new_cotizacion->seguro = 'financiado';
            $new_cotizacion->valor_seguro = $request->poliza_seguro_anual;
        }
        
        $new_cotizacion->save();

        Flash::success('Cotización creada con éxito.');

        return redirect(route('cotizacions.index'));
    }

    /**
     * Display the specified Cotizacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            Flash::error('Cotizacion not found');

            return redirect(route('cotizacions.index'));
        }

        return view('cotizacions.show')->with('cotizacion', $cotizacion);
    }

    /**
     * Show the form for editing the specified Cotizacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            Flash::error('Cotizacion not found');

            return redirect(route('cotizacions.index'));
        }

        return view('cotizacions.edit')->with('cotizacion', $cotizacion);
    }

    /**
     * Update the specified Cotizacion in storage.
     *
     * @param  int              $id
     * @param UpdateCotizacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCotizacionRequest $request)
    {
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            Flash::error('Cotizacion not found');

            return redirect(route('cotizacions.index'));
        }

        $cotizacion = $this->cotizacionRepository->update($request->all(), $id);

        Flash::success('Cotizacion updated successfully.');

        return redirect(route('cotizacions.index'));
    }

    /**
     * Remove the specified Cotizacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            Flash::error('Cotizacion not found');

            return redirect(route('cotizacions.index'));
        }

        $this->cotizacionRepository->delete($id);

        Flash::error('Cotizacion eliminada satisfactoriamente.');
        if($cotizacion->user_id == 7)
        {
            return redirect()->action('CotizacionController@cotizacionesWeb');
        }
        else
        {
            return redirect(route('cotizacions.index'));
        }
    }

    public function crearPDF($id)
    {   
        $cotizacion = \App\Models\Cotizacion::where('id','=',$id)->first();
        
        $pdf = PDF::loadView('cotizacions.pdf',['cotizacion' => $cotizacion]);
        
        return $pdf->stream();
    }

    public function pdf($id)
    
    {   $cotizacion = \App\Models\Cotizacion::where('id','=',$id)->first();
        
        return view('cotizacions.pdf')->with('cotizacion', $cotizacion);
    }

    public function cotizacionesWeb()
    {
        $cotizacions = \App\Models\Cotizacion::where('user_id','=',7)->get();
        return view('cotizacions.web')
        ->with('cotizacions', $cotizacions);
    }
}
