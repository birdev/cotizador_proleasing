<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBienAPIRequest;
use App\Http\Requests\API\UpdateBienAPIRequest;
use App\Models\Bien;
use App\Repositories\BienRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BienController
 * @package App\Http\Controllers\API
 */

class BienAPIController extends AppBaseController
{
    /** @var  BienRepository */
    private $bienRepository;

    public function __construct(BienRepository $bienRepo)
    {
        $this->bienRepository = $bienRepo;
    }

    /**
     * Display a listing of the Bien.
     * GET|HEAD /biens
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bienRepository->pushCriteria(new RequestCriteria($request));
        $this->bienRepository->pushCriteria(new LimitOffsetCriteria($request));
        $biens = $this->bienRepository->all();

        return $this->sendResponse($biens->toArray(), 'Biens retrieved successfully');
    }

    /**
     * Store a newly created Bien in storage.
     * POST /biens
     *
     * @param CreateBienAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBienAPIRequest $request)
    {
        $input = $request->all();

        $biens = $this->bienRepository->create($input);

        return $this->sendResponse($biens->toArray(), 'Bien saved successfully');
    }

    /**
     * Display the specified Bien.
     * GET|HEAD /biens/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Bien $bien */
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            return $this->sendError('Bien not found');
        }

        return $this->sendResponse($bien->toArray(), 'Bien retrieved successfully');
    }

    /**
     * Update the specified Bien in storage.
     * PUT/PATCH /biens/{id}
     *
     * @param  int $id
     * @param UpdateBienAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBienAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bien $bien */
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            return $this->sendError('Bien not found');
        }

        $bien = $this->bienRepository->update($input, $id);

        return $this->sendResponse($bien->toArray(), 'Bien updated successfully');
    }

    /**
     * Remove the specified Bien from storage.
     * DELETE /biens/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bien $bien */
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            return $this->sendError('Bien not found');
        }

        $bien->delete();

        return $this->sendResponse($id, 'Bien deleted successfully');
    }
}
