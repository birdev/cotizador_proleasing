<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateValorTasaAPIRequest;
use App\Http\Requests\API\UpdateValorTasaAPIRequest;
use App\Models\ValorTasa;
use App\Repositories\ValorTasaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ValorTasaController
 * @package App\Http\Controllers\API
 */

class ValorTasaAPIController extends AppBaseController
{
    /** @var  ValorTasaRepository */
    private $valorTasaRepository;

    public function __construct(ValorTasaRepository $valorTasaRepo)
    {
        $this->valorTasaRepository = $valorTasaRepo;
    }

    /**
     * Display a listing of the ValorTasa.
     * GET|HEAD /valorTasas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->valorTasaRepository->pushCriteria(new RequestCriteria($request));
        $this->valorTasaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $valorTasas = $this->valorTasaRepository->all();

        return $this->sendResponse($valorTasas->toArray(), 'Valor Tasas retrieved successfully');
    }

    /**
     * Store a newly created ValorTasa in storage.
     * POST /valorTasas
     *
     * @param CreateValorTasaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateValorTasaAPIRequest $request)
    {
        $input = $request->all();

        $valorTasas = $this->valorTasaRepository->create($input);

        return $this->sendResponse($valorTasas->toArray(), 'Valor Tasa saved successfully');
    }

    /**
     * Display the specified ValorTasa.
     * GET|HEAD /valorTasas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ValorTasa $valorTasa */
        $valorTasa = $this->valorTasaRepository->findWithoutFail($id);

        if (empty($valorTasa)) {
            return $this->sendError('Valor Tasa not found');
        }

        return $this->sendResponse($valorTasa->toArray(), 'Valor Tasa retrieved successfully');
    }

    /**
     * Update the specified ValorTasa in storage.
     * PUT/PATCH /valorTasas/{id}
     *
     * @param  int $id
     * @param UpdateValorTasaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateValorTasaAPIRequest $request)
    {
        $input = $request->all();

        /** @var ValorTasa $valorTasa */
        $valorTasa = $this->valorTasaRepository->findWithoutFail($id);

        if (empty($valorTasa)) {
            return $this->sendError('Valor Tasa not found');
        }

        $valorTasa = $this->valorTasaRepository->update($input, $id);

        return $this->sendResponse($valorTasa->toArray(), 'ValorTasa updated successfully');
    }

    /**
     * Remove the specified ValorTasa from storage.
     * DELETE /valorTasas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ValorTasa $valorTasa */
        $valorTasa = $this->valorTasaRepository->findWithoutFail($id);

        if (empty($valorTasa)) {
            return $this->sendError('Valor Tasa not found');
        }

        $valorTasa->delete();

        return $this->sendResponse($id, 'Valor Tasa deleted successfully');
    }
}
