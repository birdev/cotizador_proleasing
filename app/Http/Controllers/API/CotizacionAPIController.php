<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCotizacionAPIRequest;
use App\Http\Requests\API\UpdateCotizacionAPIRequest;
use App\Models\Cotizacion;
use App\Repositories\CotizacionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CotizacionController
 * @package App\Http\Controllers\API
 */

class CotizacionAPIController extends AppBaseController
{
    /** @var  CotizacionRepository */
    private $cotizacionRepository;

    public function __construct(CotizacionRepository $cotizacionRepo)
    {
        $this->cotizacionRepository = $cotizacionRepo;
    }

    /**
     * Display a listing of the Cotizacion.
     * GET|HEAD /cotizacions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cotizacionRepository->pushCriteria(new RequestCriteria($request));
        $this->cotizacionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cotizacions = $this->cotizacionRepository->all();

        return $this->sendResponse($cotizacions->toArray(), 'Cotizacions retrieved successfully');
    }

    /**
     * Store a newly created Cotizacion in storage.
     * POST /cotizacions
     *
     * @param CreateCotizacionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCotizacionAPIRequest $request)
    {
        $input = $request->all();

        $cotizacions = $this->cotizacionRepository->create($input);

        return $this->sendResponse($cotizacions->toArray(), 'Cotizacion saved successfully');
    }

    /**
     * Display the specified Cotizacion.
     * GET|HEAD /cotizacions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Cotizacion $cotizacion */
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            return $this->sendError('Cotizacion not found');
        }

        return $this->sendResponse($cotizacion->toArray(), 'Cotizacion retrieved successfully');
    }

    /**
     * Update the specified Cotizacion in storage.
     * PUT/PATCH /cotizacions/{id}
     *
     * @param  int $id
     * @param UpdateCotizacionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCotizacionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Cotizacion $cotizacion */
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            return $this->sendError('Cotizacion not found');
        }

        $cotizacion = $this->cotizacionRepository->update($input, $id);

        return $this->sendResponse($cotizacion->toArray(), 'Cotizacion updated successfully');
    }

    /**
     * Remove the specified Cotizacion from storage.
     * DELETE /cotizacions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Cotizacion $cotizacion */
        $cotizacion = $this->cotizacionRepository->findWithoutFail($id);

        if (empty($cotizacion)) {
            return $this->sendError('Cotizacion not found');
        }

        $cotizacion->delete();

        return $this->sendResponse($id, 'Cotizacion deleted successfully');
    }
}
