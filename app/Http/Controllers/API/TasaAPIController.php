<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTasaAPIRequest;
use App\Http\Requests\API\UpdateTasaAPIRequest;
use App\Models\Tasa;
use App\Repositories\TasaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TasaController
 * @package App\Http\Controllers\API
 */

class TasaAPIController extends AppBaseController
{
    /** @var  TasaRepository */
    private $tasaRepository;

    public function __construct(TasaRepository $tasaRepo)
    {
        $this->tasaRepository = $tasaRepo;
    }

    /**
     * Display a listing of the Tasa.
     * GET|HEAD /tasas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tasaRepository->pushCriteria(new RequestCriteria($request));
        $this->tasaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tasas = $this->tasaRepository->all();

        return $this->sendResponse($tasas->toArray(), 'Tasas retrieved successfully');
    }

    /**
     * Store a newly created Tasa in storage.
     * POST /tasas
     *
     * @param CreateTasaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTasaAPIRequest $request)
    {
        $input = $request->all();

        $tasas = $this->tasaRepository->create($input);

        return $this->sendResponse($tasas->toArray(), 'Tasa saved successfully');
    }

    /**
     * Display the specified Tasa.
     * GET|HEAD /tasas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Tasa $tasa */
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            return $this->sendError('Tasa not found');
        }

        return $this->sendResponse($tasa->toArray(), 'Tasa retrieved successfully');
    }

    /**
     * Update the specified Tasa in storage.
     * PUT/PATCH /tasas/{id}
     *
     * @param  int $id
     * @param UpdateTasaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTasaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Tasa $tasa */
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            return $this->sendError('Tasa not found');
        }

        $tasa = $this->tasaRepository->update($input, $id);

        return $this->sendResponse($tasa->toArray(), 'Tasa updated successfully');
    }

    /**
     * Remove the specified Tasa from storage.
     * DELETE /tasas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Tasa $tasa */
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            return $this->sendError('Tasa not found');
        }

        $tasa->delete();

        return $this->sendResponse($id, 'Tasa deleted successfully');
    }
}
