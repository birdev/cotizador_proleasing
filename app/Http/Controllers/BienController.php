<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBienRequest;
use App\Http\Requests\UpdateBienRequest;
use App\Repositories\BienRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BienController extends AppBaseController
{
    /** @var  BienRepository */
    private $bienRepository;

    public function __construct(BienRepository $bienRepo)
    {
        $this->bienRepository = $bienRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Bien.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bienRepository->pushCriteria(new RequestCriteria($request));
        $biens = $this->bienRepository->all();

        return view('biens.index')
            ->with('biens', $biens);
    }

    /**
     * Show the form for creating a new Bien.
     *
     * @return Response
     */
    public function create()
    {
        return view('biens.create');
    }

    /**
     * Store a newly created Bien in storage.
     *
     * @param CreateBienRequest $request
     *
     * @return Response
     */
    public function store(CreateBienRequest $request)
    {
        $input = $request->all();

        $bien = $this->bienRepository->create($input);

        Flash::success('Bien agregado correctamente.');

        return redirect(route('biens.index'));
    }

    /**
     * Display the specified Bien.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            Flash::error('Bien not found');

            return redirect(route('biens.index'));
        }

        return view('biens.show')->with('bien', $bien);
    }

    /**
     * Show the form for editing the specified Bien.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            Flash::error('Bien not found');

            return redirect(route('biens.index'));
        }

        return view('biens.edit')->with('bien', $bien);
    }

    /**
     * Update the specified Bien in storage.
     *
     * @param  int              $id
     * @param UpdateBienRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBienRequest $request)
    {
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            Flash::error('Bien not found');

            return redirect(route('biens.index'));
        }

        $bien = $this->bienRepository->update($request->all(), $id);

        Flash::success('Bien actualizado correctamente.');

        return redirect(route('biens.index'));
    }

    /**
     * Remove the specified Bien from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bien = $this->bienRepository->findWithoutFail($id);

        if (empty($bien)) {
            Flash::error('Bien not found');

            return redirect(route('biens.index'));
        }

        $this->bienRepository->delete($id);

        Flash::error('Bien eliminado correctamente.');

        return redirect(route('biens.index'));
    }
}
