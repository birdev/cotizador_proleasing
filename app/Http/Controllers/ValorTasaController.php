<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateValorTasaRequest;
use App\Http\Requests\UpdateValorTasaRequest;
use App\Repositories\ValorTasaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ValorTasaController extends AppBaseController
{
    /** @var  ValorTasaRepository */
    private $valorTasaRepository;

    public function __construct(ValorTasaRepository $valorTasaRepo)
    {
        $this->valorTasaRepository = $valorTasaRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the ValorTasa.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->valorTasaRepository->pushCriteria(new RequestCriteria($request));
        $valorTasas = $this->valorTasaRepository->all();

        return view('valor_tasas.index')
            ->with('valorTasas', $valorTasas);
    }

    /**
     * Show the form for creating a new ValorTasa.
     *
     * @return Response
     */
    public function create()
    {
        $valorTasa = 'NULL';
        return view('valor_tasas.create')->with('valorTasa',$valorTasa);
    }

    /**
     * Store a newly created ValorTasa in storage.
     *
     * @param CreateValorTasaRequest $request
     *
     * @return Response
     */
    public function store(CreateValorTasaRequest $request)
    {
        $new_valor = new \App\Models\ValorTasa();        

        $const="";
        foreach($request->bien_section as $var)
        {
            $const.=','.($var);
        }
        $const = trim($const, ',');

        if($request->plazo_vr != null)
        {
            $aux = "";
            foreach($request->plazo_vr as $var)
            {
                $aux.=','.($var);
            }
            $aux = trim($aux,',');

            $new_valor->plazo_vr = $aux;
        }
        else{

        }
        
        $new_valor->tasa_id = $request->tasa;
        $new_valor->valor = $request->valor;
        $new_valor->bien_section = $const;
        $new_valor->save();

        Flash::success('Valor agregado a la tasa');

        return redirect(route('valorTasas.index'));
    }

    /**
     * Display the specified ValorTasa.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $valorTasa = $this->valorTasaRepository->findWithoutFail($id);

        if (empty($valorTasa)) {
            Flash::error('Valor Tasa not found');

            return redirect(route('valorTasas.index'));
        }

        return view('valor_tasas.show')->with('valorTasa', $valorTasa);
    }

    /**
     * Show the form for editing the specified ValorTasa.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $valorTasa = $this->valorTasaRepository->findWithoutFail($id);

        if (empty($valorTasa)) {
            Flash::error('Valor Tasa not found');

            return redirect(route('valorTasas.index'));
        }

        return view('valor_tasas.edit')->with('valorTasa', $valorTasa);
    }

    /**
     * Update the specified ValorTasa in storage.
     *
     * @param  int              $id
     * @param UpdateValorTasaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateValorTasaRequest $request)
    {
        
        $valorTasa = \App\Models\ValorTasa::find($id);
        
        if (empty($valorTasa)) {
            Flash::error('Valor Tasa not found');

            return redirect(route('valorTasas.index'));
        } 

        $const="";
        foreach($request->bien_section as $var)
        {
            $const.=','.($var);
        }
        $const = trim($const, ',');
        
        if($request->plazo_vr != null)
        {
            $aux="";
            foreach($request->plazo_vr as $variable)
            {
                $aux.=','.($variable);
            }
            $aux = trim($aux,',');
            
            $valorTasa->plazo_vr = $aux;
        }
        else{

        }

        $valorTasa->tasa_id = $request->tasaIDupdate;
        $valorTasa->valor = $request->valor;
        $valorTasa->bien_section = $const;
        $valorTasa->save();

        Flash::success('Valor de la tasa actualizado');

        return redirect(route('valorTasas.index'));
    }

    /**
     * Remove the specified ValorTasa from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $valorTasa = $this->valorTasaRepository->findWithoutFail($id);

        if (empty($valorTasa)) {
            Flash::error('Valor Tasa not found');

            return redirect(route('valorTasas.index'));
        }

        $this->valorTasaRepository->delete($id);

        Flash::success('Valor de la tasa eliminado');

        return redirect(route('valorTasas.index'));
    }
}
