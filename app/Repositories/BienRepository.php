<?php

namespace App\Repositories;

use App\Models\Bien;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BienRepository
 * @package App\Repositories
 * @version November 9, 2018, 6:06 pm UTC
 *
 * @method Bien findWithoutFail($id, $columns = ['*'])
 * @method Bien find($id, $columns = ['*'])
 * @method Bien first($columns = ['*'])
*/
class BienRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'update_at',
        'delated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bien::class;
    }
}
