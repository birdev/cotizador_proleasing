<?php

namespace App\Repositories;

use App\Models\Tasa;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TasaRepository
 * @package App\Repositories
 * @version November 9, 2018, 6:06 pm UTC
 *
 * @method Tasa findWithoutFail($id, $columns = ['*'])
 * @method Tasa find($id, $columns = ['*'])
 * @method Tasa first($columns = ['*'])
*/
class TasaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tasa::class;
    }
}
