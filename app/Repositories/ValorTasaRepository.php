<?php

namespace App\Repositories;

use App\Models\ValorTasa;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ValorTasaRepository
 * @package App\Repositories
 * @version November 9, 2018, 6:06 pm UTC
 *
 * @method ValorTasa findWithoutFail($id, $columns = ['*'])
 * @method ValorTasa find($id, $columns = ['*'])
 * @method ValorTasa first($columns = ['*'])
*/
class ValorTasaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tasa_id',
        'valor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ValorTasa::class;
    }
}
