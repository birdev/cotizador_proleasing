<?php

use App\Models\ValorTasa;
use App\Repositories\ValorTasaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ValorTasaRepositoryTest extends TestCase
{
    use MakeValorTasaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ValorTasaRepository
     */
    protected $valorTasaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->valorTasaRepo = App::make(ValorTasaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateValorTasa()
    {
        $valorTasa = $this->fakeValorTasaData();
        $createdValorTasa = $this->valorTasaRepo->create($valorTasa);
        $createdValorTasa = $createdValorTasa->toArray();
        $this->assertArrayHasKey('id', $createdValorTasa);
        $this->assertNotNull($createdValorTasa['id'], 'Created ValorTasa must have id specified');
        $this->assertNotNull(ValorTasa::find($createdValorTasa['id']), 'ValorTasa with given id must be in DB');
        $this->assertModelData($valorTasa, $createdValorTasa);
    }

    /**
     * @test read
     */
    public function testReadValorTasa()
    {
        $valorTasa = $this->makeValorTasa();
        $dbValorTasa = $this->valorTasaRepo->find($valorTasa->id);
        $dbValorTasa = $dbValorTasa->toArray();
        $this->assertModelData($valorTasa->toArray(), $dbValorTasa);
    }

    /**
     * @test update
     */
    public function testUpdateValorTasa()
    {
        $valorTasa = $this->makeValorTasa();
        $fakeValorTasa = $this->fakeValorTasaData();
        $updatedValorTasa = $this->valorTasaRepo->update($fakeValorTasa, $valorTasa->id);
        $this->assertModelData($fakeValorTasa, $updatedValorTasa->toArray());
        $dbValorTasa = $this->valorTasaRepo->find($valorTasa->id);
        $this->assertModelData($fakeValorTasa, $dbValorTasa->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteValorTasa()
    {
        $valorTasa = $this->makeValorTasa();
        $resp = $this->valorTasaRepo->delete($valorTasa->id);
        $this->assertTrue($resp);
        $this->assertNull(ValorTasa::find($valorTasa->id), 'ValorTasa should not exist in DB');
    }
}
