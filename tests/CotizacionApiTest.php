<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CotizacionApiTest extends TestCase
{
    use MakeCotizacionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCotizacion()
    {
        $cotizacion = $this->fakeCotizacionData();
        $this->json('POST', '/api/v1/cotizacions', $cotizacion);

        $this->assertApiResponse($cotizacion);
    }

    /**
     * @test
     */
    public function testReadCotizacion()
    {
        $cotizacion = $this->makeCotizacion();
        $this->json('GET', '/api/v1/cotizacions/'.$cotizacion->id);

        $this->assertApiResponse($cotizacion->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCotizacion()
    {
        $cotizacion = $this->makeCotizacion();
        $editedCotizacion = $this->fakeCotizacionData();

        $this->json('PUT', '/api/v1/cotizacions/'.$cotizacion->id, $editedCotizacion);

        $this->assertApiResponse($editedCotizacion);
    }

    /**
     * @test
     */
    public function testDeleteCotizacion()
    {
        $cotizacion = $this->makeCotizacion();
        $this->json('DELETE', '/api/v1/cotizacions/'.$cotizacion->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cotizacions/'.$cotizacion->id);

        $this->assertResponseStatus(404);
    }
}
