<?php

use Faker\Factory as Faker;
use App\Models\ValorTasa;
use App\Repositories\ValorTasaRepository;

trait MakeValorTasaTrait
{
    /**
     * Create fake instance of ValorTasa and save it in database
     *
     * @param array $valorTasaFields
     * @return ValorTasa
     */
    public function makeValorTasa($valorTasaFields = [])
    {
        /** @var ValorTasaRepository $valorTasaRepo */
        $valorTasaRepo = App::make(ValorTasaRepository::class);
        $theme = $this->fakeValorTasaData($valorTasaFields);
        return $valorTasaRepo->create($theme);
    }

    /**
     * Get fake instance of ValorTasa
     *
     * @param array $valorTasaFields
     * @return ValorTasa
     */
    public function fakeValorTasa($valorTasaFields = [])
    {
        return new ValorTasa($this->fakeValorTasaData($valorTasaFields));
    }

    /**
     * Get fake data of ValorTasa
     *
     * @param array $postFields
     * @return array
     */
    public function fakeValorTasaData($valorTasaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tasa_id' => $fake->randomDigitNotNull,
            'valor' => $fake->word
        ], $valorTasaFields);
    }
}
