<?php

use Faker\Factory as Faker;
use App\Models\Bien;
use App\Repositories\BienRepository;

trait MakeBienTrait
{
    /**
     * Create fake instance of Bien and save it in database
     *
     * @param array $bienFields
     * @return Bien
     */
    public function makeBien($bienFields = [])
    {
        /** @var BienRepository $bienRepo */
        $bienRepo = App::make(BienRepository::class);
        $theme = $this->fakeBienData($bienFields);
        return $bienRepo->create($theme);
    }

    /**
     * Get fake instance of Bien
     *
     * @param array $bienFields
     * @return Bien
     */
    public function fakeBien($bienFields = [])
    {
        return new Bien($this->fakeBienData($bienFields));
    }

    /**
     * Get fake data of Bien
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBienData($bienFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'update_at' => $fake->date('Y-m-d H:i:s'),
            'delated_at' => $fake->date('Y-m-d H:i:s')
        ], $bienFields);
    }
}
