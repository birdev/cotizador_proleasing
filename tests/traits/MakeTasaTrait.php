<?php

use Faker\Factory as Faker;
use App\Models\Tasa;
use App\Repositories\TasaRepository;

trait MakeTasaTrait
{
    /**
     * Create fake instance of Tasa and save it in database
     *
     * @param array $tasaFields
     * @return Tasa
     */
    public function makeTasa($tasaFields = [])
    {
        /** @var TasaRepository $tasaRepo */
        $tasaRepo = App::make(TasaRepository::class);
        $theme = $this->fakeTasaData($tasaFields);
        return $tasaRepo->create($theme);
    }

    /**
     * Get fake instance of Tasa
     *
     * @param array $tasaFields
     * @return Tasa
     */
    public function fakeTasa($tasaFields = [])
    {
        return new Tasa($this->fakeTasaData($tasaFields));
    }

    /**
     * Get fake data of Tasa
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTasaData($tasaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $tasaFields);
    }
}
