<?php

use App\Models\Cotizacion;
use App\Repositories\CotizacionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CotizacionRepositoryTest extends TestCase
{
    use MakeCotizacionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CotizacionRepository
     */
    protected $cotizacionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cotizacionRepo = App::make(CotizacionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCotizacion()
    {
        $cotizacion = $this->fakeCotizacionData();
        $createdCotizacion = $this->cotizacionRepo->create($cotizacion);
        $createdCotizacion = $createdCotizacion->toArray();
        $this->assertArrayHasKey('id', $createdCotizacion);
        $this->assertNotNull($createdCotizacion['id'], 'Created Cotizacion must have id specified');
        $this->assertNotNull(Cotizacion::find($createdCotizacion['id']), 'Cotizacion with given id must be in DB');
        $this->assertModelData($cotizacion, $createdCotizacion);
    }

    /**
     * @test read
     */
    public function testReadCotizacion()
    {
        $cotizacion = $this->makeCotizacion();
        $dbCotizacion = $this->cotizacionRepo->find($cotizacion->id);
        $dbCotizacion = $dbCotizacion->toArray();
        $this->assertModelData($cotizacion->toArray(), $dbCotizacion);
    }

    /**
     * @test update
     */
    public function testUpdateCotizacion()
    {
        $cotizacion = $this->makeCotizacion();
        $fakeCotizacion = $this->fakeCotizacionData();
        $updatedCotizacion = $this->cotizacionRepo->update($fakeCotizacion, $cotizacion->id);
        $this->assertModelData($fakeCotizacion, $updatedCotizacion->toArray());
        $dbCotizacion = $this->cotizacionRepo->find($cotizacion->id);
        $this->assertModelData($fakeCotizacion, $dbCotizacion->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCotizacion()
    {
        $cotizacion = $this->makeCotizacion();
        $resp = $this->cotizacionRepo->delete($cotizacion->id);
        $this->assertTrue($resp);
        $this->assertNull(Cotizacion::find($cotizacion->id), 'Cotizacion should not exist in DB');
    }
}
