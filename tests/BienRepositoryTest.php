<?php

use App\Models\Bien;
use App\Repositories\BienRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BienRepositoryTest extends TestCase
{
    use MakeBienTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BienRepository
     */
    protected $bienRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bienRepo = App::make(BienRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBien()
    {
        $bien = $this->fakeBienData();
        $createdBien = $this->bienRepo->create($bien);
        $createdBien = $createdBien->toArray();
        $this->assertArrayHasKey('id', $createdBien);
        $this->assertNotNull($createdBien['id'], 'Created Bien must have id specified');
        $this->assertNotNull(Bien::find($createdBien['id']), 'Bien with given id must be in DB');
        $this->assertModelData($bien, $createdBien);
    }

    /**
     * @test read
     */
    public function testReadBien()
    {
        $bien = $this->makeBien();
        $dbBien = $this->bienRepo->find($bien->id);
        $dbBien = $dbBien->toArray();
        $this->assertModelData($bien->toArray(), $dbBien);
    }

    /**
     * @test update
     */
    public function testUpdateBien()
    {
        $bien = $this->makeBien();
        $fakeBien = $this->fakeBienData();
        $updatedBien = $this->bienRepo->update($fakeBien, $bien->id);
        $this->assertModelData($fakeBien, $updatedBien->toArray());
        $dbBien = $this->bienRepo->find($bien->id);
        $this->assertModelData($fakeBien, $dbBien->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBien()
    {
        $bien = $this->makeBien();
        $resp = $this->bienRepo->delete($bien->id);
        $this->assertTrue($resp);
        $this->assertNull(Bien::find($bien->id), 'Bien should not exist in DB');
    }
}
