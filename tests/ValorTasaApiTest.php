<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ValorTasaApiTest extends TestCase
{
    use MakeValorTasaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateValorTasa()
    {
        $valorTasa = $this->fakeValorTasaData();
        $this->json('POST', '/api/v1/valorTasas', $valorTasa);

        $this->assertApiResponse($valorTasa);
    }

    /**
     * @test
     */
    public function testReadValorTasa()
    {
        $valorTasa = $this->makeValorTasa();
        $this->json('GET', '/api/v1/valorTasas/'.$valorTasa->id);

        $this->assertApiResponse($valorTasa->toArray());
    }

    /**
     * @test
     */
    public function testUpdateValorTasa()
    {
        $valorTasa = $this->makeValorTasa();
        $editedValorTasa = $this->fakeValorTasaData();

        $this->json('PUT', '/api/v1/valorTasas/'.$valorTasa->id, $editedValorTasa);

        $this->assertApiResponse($editedValorTasa);
    }

    /**
     * @test
     */
    public function testDeleteValorTasa()
    {
        $valorTasa = $this->makeValorTasa();
        $this->json('DELETE', '/api/v1/valorTasas/'.$valorTasa->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/valorTasas/'.$valorTasa->id);

        $this->assertResponseStatus(404);
    }
}
