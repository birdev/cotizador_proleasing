<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TasaApiTest extends TestCase
{
    use MakeTasaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTasa()
    {
        $tasa = $this->fakeTasaData();
        $this->json('POST', '/api/v1/tasas', $tasa);

        $this->assertApiResponse($tasa);
    }

    /**
     * @test
     */
    public function testReadTasa()
    {
        $tasa = $this->makeTasa();
        $this->json('GET', '/api/v1/tasas/'.$tasa->id);

        $this->assertApiResponse($tasa->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTasa()
    {
        $tasa = $this->makeTasa();
        $editedTasa = $this->fakeTasaData();

        $this->json('PUT', '/api/v1/tasas/'.$tasa->id, $editedTasa);

        $this->assertApiResponse($editedTasa);
    }

    /**
     * @test
     */
    public function testDeleteTasa()
    {
        $tasa = $this->makeTasa();
        $this->json('DELETE', '/api/v1/tasas/'.$tasa->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/tasas/'.$tasa->id);

        $this->assertResponseStatus(404);
    }
}
