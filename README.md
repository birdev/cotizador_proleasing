
## Datos de accesos y pruebas

En este apartado deben ir los datos de acceso para pruebas, usuario con y sin privilegios.
-----------------------------------------------------------------------------------------------
usuario: dgonzalez@birdev.mx

contraseña : 123123

tipo de usuario: administrador

Descripción: Este usuario tendrá el control total de la plataforma, podrá crear cotizaciones, administrar usuarios y los valores de las tasas.
Podrá ver las cotizaciones que se realicen en el cotizador web.

Links:

http://162.243.136.245/cotizador_proleasing/public/cotizacions - modulo de cotizaciones individuales por empleado

http://162.243.136.245/cotizador_proleasing/public/cotizacions/create - modulo para la creación de una cotizacion

http://162.243.136.245/cotizador_proleasing/public/cotizaciones/web - Modulo para visualizar las cotizaciones que se hayan realizado en el cotizador publico

http://162.243.136.245/cotizador_proleasing/public/users - Administración de usuarios

http://162.243.136.245/cotizador_proleasing/public/valorTasas - Administración de los valores de las tasas.

-----------------------------------------------------------------------------------------------

usuario: jesus@birdev.mx

contraseña : 123123

tipo de usuario: empleado

Descripción: Este usuario esta limitado a solo poder crear cotizaciones individuales para sus clientes.

Links:

http://162.243.136.245/cotizador_proleasing/public/cotizacions - Modulo de cotizaciones

http://162.243.136.245/cotizador_proleasing/public/cotizacions/create - Modulo para crear cotizaciones


-----------------------------------------------------------------------------------------------

usuario: Web

contraseña : N/A

tipo de usuario: Publico

Descripcion: Este usuario aplica para todo el publico en general que acceda al cotizador publico, podrán solicitar cotizaciones.

http://162.243.136.245/cotizador_proleasing/public/inicio - Modulo de cotizador publico.

-----------------------------------------------------------------------------------------------




## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).


