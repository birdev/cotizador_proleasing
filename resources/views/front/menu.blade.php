<style>
.listado{
    font-size: 15px;
    padding: 20px 0px 0px 0px;
    color: white;
    font-weight: 600;
}

.lista{
width:80%;
}

.iconos-menu{
    float: left;
    margin-right: 10px;
}

.link{
color: white;
}

.link:hover{
    color:#ec7d00;
    text-decoration: none;
}

.active{
    color:#ec7d00;
}

</style>
<div class="" style="background-color: #333331 !important">
    <ul class="nav flex-column lista">
        <li class="listado"><a class="link" href="http://www.pro-leasing.mx/nosotros.php">Nosotros</a></li>
        <li class="listado"><a class="link" href="http://www.pro-leasing.mx/que-puedes-arrendar.php">¿Qué puedes arrendar?</a></li>
        <li class="listado"><a class="link" href="http://www.pro-leasing.mx/ventajas.php">Ventajas</a></li>
        <li class="listado"><a class="link active" href="#">Cotizador</a></li>
        <li class="listado"><a class="link" href="http://www.pro-leasing.mx/clientes.php">Clientes</a></li>
        <li class="listado"><a class="link" href="http://www.pro-leasing.mx/descargas.php">Descargas</a></li>
        <li class="listado"><a class="link" href="http://www.pro-leasing.mx/glosario.php">ABC Finanzas</a></li>
        <li class="listado"><a class="link" href="{!! asset('/login') !!}">Acceder</a></li>
        <li class="listado" >
            <img src="http://www.pro-leasing.mx/images/home-ico-1.png" width="22" height="22" alt="" class="iconos-menu"> 
            <a href="https://www.facebook.com/ProLeasing/" target="_blank" style="margin:0; padding:0; float:left"><img src="http://www.pro-leasing.mx/images/home-ico-2.png" width="22" height="22" alt="" class="iconos-menu"></a>
            <a href="https://www.youtube.com/channel/UCP-t6LeGPZv9-ovfqWHiErQ " target="_blank" style="margin:0; padding:0; float:left"><img src="http://www.pro-leasing.mx/images/home-ico-3.png" width="22" height="22" alt="" class="iconos-menu"></a>
        </li>
    </ul>
</div>