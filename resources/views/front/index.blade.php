<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="{!! asset('imagenes/favicon.ico') !!}" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cotizador Proleasing</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="{!! asset('assets/styles/shards-dashboards.1.1.0.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/styles/extras.1.1.0.min.css') !!}">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

</head>
<style>

    #contenido-general{
        padding:10px 100px;
    }

    label{
        font-family: 'Roboto', sans-serif;
        font-weight: normal;
        color:#818181;
    }

    .errorField{
        color: #000;
        font-family: 'Roboto', sans-serif;
        padding-left: 10px;
        font-weight: 900 !important;
    }

    .page-title{
        color: #ff6600 !important;
        font-family: 'Roboto', sans-serif;
        font-size: 36pt !important;
        font-weight: 800 !important;
    }
    .page-subtitle{
        color: #818181 !important;
        letter-spacing: 0.01em !important;
        font-weight: 100 !important;
        font-family: 'Roboto', sans-serif;
        font-size: 12px !important;
    }

    .form-control{
        border-radius: 40px !important;
    }

    select.prueba {
        -webkit-appearance: none;
        -moz-appearance: none;
    color: #ff6600;
    background: url({!! asset('imagenes/flecha2.png') !!}) no-repeat right 0px top -4px;
    background-color: #fff;
    -webkit-border-radius: 40px !important;
    -moz-border-radius: 40px !important;
    border-radius: 40px !important;
    padding-left: 15px;
    height: calc(2.09375rem + 2px);
    padding: .4375rem .75rem;
    font-size: .8125rem;
    line-height: 1.5;
    background-clip: padding-box;
    border: 1px solid #e1e5eb;
        font-weight: 300;
        will-change: border-color,box-shadow;
        box-shadow: none;
        transition: box-shadow 250ms cubic-bezier(.27,.01,.38,1.06),border 250ms cubic-bezier(.27,.01,.38,1.06);
        display: block;
        width: 100%;
        background-clip: padding-box;
        text-transform: none;
    }

    .custom-form{
        -webkit-appearance: none;
        -moz-appearance: none;
        border-top: none;
        border-left: none;
        border-right: none;
        border-radius: 0px !important;
        font-family: 'Roboto', sans-serif;
        font-size: 16px !important;
        color: #ff6600 !important;
        font-weight: 500 !important;
        border-bottom-color: #c6c6c5;
        text-align: center;
    }

    /* .custom-form:focus{
        border-color: #ff6800;
    }

    .custom-form:focus:hover{
        border-color: #ff6800;
    } */

    .form-control:focus{
        border-color: #ff6800;
        box-shadow: none;
    }

    .form-control:focus:hover{
        border-color: #ff6800;
    }

    select.prueba:focus{
        border-color: #ff6800;
        outline: none;
    }

    select.prueba:focus:hover{
        border-color: #ff6800;
        outline: none;
    }

    .customize{
        margin: 20px 0px;
    }

    .customize .ui-state-default, .ui-widget-content .ui-state-default{
        border-radius: 100px;
        border: 4px solid #ffffff;
        background: #ff6800;
    }

    .ui-widget-content .ui-state-default:focus{
        outline: none;
        border: 0px solid #ffffff;
    }

    .ui-widget-content .ui-state-default:focus:hover{
        outline: none;
        border: 0px solid #ffffff;
    }

    .custom-control-input:focus~.custom-control-label::before{
        box-shadow: none;
        
    }

    .custom-control-input:active~.custom-control-label::before{
        background-color: transparent;
    }

    .botonEnviar:focus{
        outline: none;
    }
    
    .customize .ui-slider-range { 
        background: #818181; 
    }

    .ui-widget.ui-widget-content{
        border: 1px solid #fff !important;
    }

    .ui-slider-horizontal{
    height:4px;
    background-color: #ff6600;
    }

    .ui-slider-horizontal .ui-slider-handle{
        top: -0.5em;
    }

    .valoresNaranjas{
        color: #ff6600 !important;
        font-family: 'Roboto', sans-serif;
        font-size: 36px !important;
        font-weight: 500 !important;
    }

    .custom-radio .custom-control-input:checked~.custom-control-label::before{
        background-color: #ff6800 !important;
    }

    .custom-checkbox .custom-control-input:checked~.custom-control-label::before{
        background-color: #ff6800 !important;
    }

    .botonEnviar{
        background: #ff6600;
        padding: 12px 50px;
        border-radius: 30px;
        color: #fff;
        font-family: 'Roboto', sans-serif;
        font-size: 14px !important;
        font-weight: 100;
        text-transform: uppercase;
        cursor: pointer;
    }

    a:hover{
        text-decoration: none;
        color:white;
    }

    body{
        /* padding: 10px 100px; */
    }
    @media only screen and (max-width: 480px) {
        #contenido-general{
            padding:10px;
        }
        #lineaVertical{
            display:none;
        }

        .row .col-sm-12 .form-group{
            margin-right: 0px;
            margin-left: 0px;
        }
    }
</style>
<body class="h-100" style="background-color: #fff;">
<?php
$bienes = \App\Models\Bien::all();
?>
{!! Form::open(['route' => 'front.enviar_cotizacion', 'id' => 'formularioXD']) !!}
<!-- --------------------CONDICIONES DE ARRENDAMIENTO---------------------------------------------------------------- -->
<!-- --------------------DATOS DEL BIEN A ARRENDAR---------------------------------------------------------------- -->
<div class="container-fluid">
    <div class="row" >
            <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0" style="background-color: #333331 !important; box-shadow:none !important;">
            <div class="main-navbar" style="padding: 30px 0px 0px 30px;">
                <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0" style="background-color: #333331 !important;border-bottom: none !important;">
                    <a class="navbar-brand w-100 mr-0" href="http://www.pro-leasing.mx/" style="line-height: 25px;">
                    <div class="d-table m-auto">
                        <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 100%;" src="{!! asset('imagenes/logo.png') !!}" alt="Shards Dashboard">
                        <!-- <span class="d-none d-md-inline ml-1">Proleasing</span> -->
                    </div>
                    </a>
                    <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                    <i class="material-icons">&#xE5C4;</i>
                    </a>
                </nav>
            </div>
            <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
            <div class="input-group input-group-seamless" style="padding: 10px 0px 0px 50px;">
                <div class="input-group-prepend">
                <div class="input-group-text">
                    <!-- <i class="fas fa-search"></i> -->
                </div>
                </div>            
            </form>
            <div class="nav-wrapper">
                <ul class="nav flex-column">
                    @include('front.menu')
                </ul>
            </div>
        </aside>
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
            @include('front.top')
<!-- Content Wrapper. Contains page content -->
            <div class="main-content-container container-fluid" id="contenido-general">
                <div class="row">                    
                    <!-- aqui inicia el chido -->
                    <div class="form-group col-sm-12">
                        <div class="page-header row no-gutters py-4">
                            <div class="col-12 col-sm-12 text-center text-sm-left mb-0">
                            <span class="text-uppercase page-subtitle" id="tope">Condiciones de arrendamiento</span>
                            <h3 class="page-title">Datos del bien</h3>
                            </div>
                        </div>    
                    </div>
                    <div class="col-sm-12" style="padding-bottom: 30px; top: -15px;">
                    <div style="width: 100%;
                        height: 2px;
                        background: linear-gradient(270deg, rgba(48,48,48,1) 50%, rgba(255,102,0,1) 50%);"></div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <!-- Bien Id Field -->
                        <div class="form-group col-sm-3">
                            {!! Form::label('bien_id', 'Bien:') !!}
                            <!-- {!! Form::number('bien_id', null, ['class' => 'form-control']) !!} -->
                            <select class="prueba" style="font-size: 12px;" id="selectBienes" name="bien" onchange="tipoBien(this.value)" required>
                                <option value="" disabled selected>Selecciona un bien</option>
                                    @foreach($bienes as $bien)                
                                            <option value='{!! $bien->nombre !!}'>{!! $bien->nombre !!}</option>
                                    @endforeach
                            </select>   
                            <span class="errorField" id="eBien"></span>
                        </div>
                        <!-- onchange="redireccionar(this.value)" -->
                        <!-- Marca Field -->
                        <div class="form-group col-sm-3">
                            {!! Form::label('marca', 'Marca:') !!}
                            {!! Form::text('marca', null, ['class' => 'form-control','required' => 'required']) !!}
                            <span class="errorField" id="eMarca"></span>
                        </div>

                        <!-- Modelo Field -->
                        <div class="form-group col-sm-3">
                            {!! Form::label('modelo', 'Modelo:') !!}
                            {!! Form::text('modelo', null, ['class' => 'form-control','required' => 'required']) !!}
                            <span class="errorField" id="eModelo"></span>
                        </div>

                        <!-- Año Field -->
                        <div class="form-group col-sm-3">
                            {!! Form::label('anio', 'Año:') !!}
                            <!-- {!! Form::number('anio', null, ['class' => 'form-control','required' => 'required']) !!} -->
                            <select class="prueba" id="anio" name="anio">
                                <option value="" disabled selected>Selecciona un año</option>
                            </select>   
                            <span class="errorField" id="eAnio"></span>
                        </div>
                    </div>


                    <div class="form-group col-sm-12 row">
                        <!-- Valor Factura Sin Iva Field -->
                        <div class="form-group col-sm-3">
                        </div>
                        <div class="form-group col-sm-3">
                            <center>{!! Form::label('valor_factura_con_iva', 'Valor factura con IVA:') !!}</center>
                            {!! Form::text('valor_factura_con_iva', '$' , ['class' => 'form-control custom-form','required' => 'required']) !!}
                            <span id="eValorFacturaConIva" class="errorField"></span>
                        </div>
                        
                        <div class="form-group col-sm-3">
                                <center>{!! Form::label('valor_factura_sin_iva', 'Valor factura sin IVA:') !!}</center>
                                {!! Form::hidden('valor_factura_sin_iva', null, ['class' => 'form-control custom-form','readonly' => 'readonly','step'=> 'any','style' => 'background-color: #f5f6f700 !important;']) !!}
                                {!! Form::text('valor_factura_sin_iva2', '$', ['id'=> 'valor_factura_sin_iva2' , 'class' => 'form-control custom-form','readonly' => 'readonly','step'=> 'any','style' => 'background-color: #f5f6f700 !important;']) !!}
                            </div>
                        </div>
                        <div class="form-group col-sm-3"></div>
                    </div>

                    <div class="col-sm-12" style="padding: 30px 0px">
                    <div style="width: 100%;
                        height: 1px;
                        background: #d4d4d4"></div>
                    </div>

                    <!-- --------------------DATOS DEL BIEN A ARRENDAR---------------------------------------------------------------- -->
                    <div class="form-group col-sm-12">
                        <div class="page-header row no-gutters py-4">
                            <div class="col-12 col-sm-12 text-center text-sm-left mb-0">
                            <span class="text-uppercase page-subtitle">Condiciones de arrendamiento</span>
                            <h3 class="page-title">Ajuste de valores</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12" style="padding-bottom: 30px; top: -15px;">
                    <div style="width: 100%;
                        height: 2px;
                        background: linear-gradient(270deg, rgba(48,48,48,1) 50%, rgba(255,102,0,1) 50%);"></div>
                    </div>
                    <div class="form-group col-sm-12 row">
                        <!-- Plazo Field -->
                        <div class="form-group col-sm-5" style="text-aling:center">
                            <center>{!! Form::label('plazo', 'Plazo:') !!}</center>
                            <!-- {!! Form::number('plazo', null, ['class' => 'form-control']) !!} -->
                            <div class="customize" id="sliderPlazo"></div> 
                            <center><h1 class="valoresNaranjas" id="valoresPlazo">0</h1></center>
                            <h1 id="valorPlazoOculto" style="display:none"></h1>
                            {!! Form::number('plazo', null, ['class' => 'form-control', 'style' => 'display:none']) !!}    
                        </div>
                        <div id="lineaVertical" class="form-group col-sm-2">
                            <div style="
                            width: 80%;
                            height: 1px;
                            background: #d4d4d4;
                            transform: rotate(270deg );
                            margin-top: 50px;
                            ">
                            </div>
                        </div>
                        <!-- Porcentaje Pago Inicial Field -->
                        <div class="form-group col-sm-5" style="text-align:center">
                            <center>{!! Form::label('porcentaje_pago_inicial', 'Porcentaje pago inicial:') !!}</center>
                            <div class="customize" id="sliderPagoInicial"></div> 
                            <center><h1 class="valoresNaranjas" id="valoresPagoInicial">0%</h1></center>
                            <h1 id="valorPagoInicialOculto" style="display:none"></h1>
                            {!! Form::number('porcentaje_pago_inicial', null, ['class' => 'form-control','style' => 'display:none']) !!}    
                        </div>
                    </div>


                    {!! Form::number('valor_residual_calculo', null, ['id' => 'valor_residual_calculo','class' => 'form-control','style' => 'display:none']) !!}


                    <div class="form-group col-sm-12 row">
                        <div class="form-group col-sm-6">
                            {!! Form::label('poliza_seguro_anual', 'Poliza de seguro anual: ') !!}                       
                            {!! Form::hidden('poliza_seguro_anual', 0, ['id' => 'poliza_seguro_anual','class' => 'form-control custom-form','style' => 'width:150px','required' => 'required']) !!}
                            {!! Form::text('poliza_seguro_anual2', '$0', ['id' => 'poliza_seguro_anual2','class' => 'form-control custom-form','style' => 'width:150px','required' => 'required']) !!}
                            <span id="ePoliza" class="errorField"></span>
                            <div>
                                <div class="custom-control custom-radio mb-1">
                                    <input type="radio" id="formContado" name="formRadios" class="custom-control-input" checked value="contado" onclick="polizaSeguroAnual();actualizarValores();">
                                    <label class="custom-control-label" for="formContado">Contado</label>
                                </div>
                                <div class="custom-control custom-radio mb-1">
                                    <input type="radio" id="formFinanciado" name="formRadios" class="custom-control-input" value="financiado" onclick="polizaSeguroAnual();actualizarValores();">
                                    <label class="custom-control-label" for="formFinanciado">Financiado</label>
                                </div>
                            </div>
                        </div>        
                        <div class="form-group col-sm-12">
                            <a class="botonEnviar" style="padding: 6px 16px;font-size: 12px !important;" href="https://autos.interesse.com.mx/telerentaventas" target="_blank">Cotizar en linea</a>
                        </div>
                    </div>
                    <div id="localizadorValidado" style="display:none">
                        <div class="form-group col-sm-12 row">
                            <div class="form-group col-sm-6">
                                {!! Form::label('localizador', 'Localizador(costo mensual): ') !!}
                                <br>
                                <div class="custom-control custom-checkbox mb-1">
                                    <input type="checkbox" class="custom-control-input" id="formLocalizador" name="formLocalizador" value="localizador" onclick="localizadorCostoMensual();actualizarValores();">
                                    <label class="custom-control-label" for="formLocalizador">Agregar</label>
                                </div>        
                            </div>
                        </div>
                    </div>

                    <!-- --------------------VALORS RESULTANTES ---------------------------------------------------------------- -->


                                {!! Form::hidden('pago_inicial', null, ['id' => 'pago_inicial','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                        
                                {!! Form::hidden('gastos_de_apertura', null, ['id' => 'gastos_de_apertura','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                        
                                {!! Form::hidden('pago_mensual_fijo_sin_iva', null, ['id' => 'pago_mensual_fijo_sin_iva','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                        
                                {!! Form::hidden('renta_en_deposito', null, ['id' => 'renta_en_deposito','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                        
                                {!! Form::hidden('subtotal', null, ['id' => 'subtotal','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                        
                                {!! Form::hidden('iva', null, ['id' =>  'iva','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                            
                                {!! Form::hidden('valor_residual_sin_iva', null, ['id' => 'valor_residual_sin_iva','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                            
                                {!! Form::hidden('total_pago_inicial', null, ['id' => 'total_pago_inicial','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
                        
                                {!! Form::hidden('pago_mensual_fijo', null, ['id' => 'pago_mensual_fijo','class' => 'form-control','style' => 'visibility:hidden','readonly' => 'readonly','step'=> 'any']) !!}    
                                
                                {!! Form::hidden('porcentaje_comision', null, ['id' => 'porcentaje_comision','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}

                                {!! Form::hidden('porcentaje_adicional_vr', null, ['id' => 'porcentaje_adicional_vr','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}

                                {!! Form::hidden('porcentaje_tasa', null, ['id' => 'porcentaje_tasa','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12" style="text-align:center">
                        <!-- {!! Form::submit('Cotizar', ['class' => 'botonEnviar']) !!} -->
                        <!-- Button trigger modal -->
                    <button style="margin-bottom:50px" type="button" id="popup" class="botonEnviar" data-toggle="modal" data-target="#exampleModalCenter">
                    Cotizar
                    </button>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title page-title" style="font-size:25px !important;" id="exampleModalCenterTitle">Ingresa tus datos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <!-- Nombre Cliente Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('nombre_cliente', 'Nombre:') !!}
                                        <input type="text" name="nombre_cliente" id="nombre_cliente" class=" form-control" required minlength="3">
                                        <span id="eNombreCliente" class="errorField"></span>
                                    </div>

                                    <!-- Apellido Paterno Cliente Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('apellido_paterno_cliente', 'Apellido paterno:') !!}
                                        <input type="text" name="apellido_paterno_cliente" id="apellido_paterno_cliente" class="form-control" required minlength="4">
                                        <span id="eApePat" class="errorField"></span>
                                    </div>

                                    <!-- Apellido Materno Cliente Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('apellido_materno_cliente', 'Apellido materno:') !!}
                                        <input type="text" name="apellido_materno_cliente" id="apellido_materno_cliente" class="form-control" required minlength="4">
                                        <span id="eApeMat" class="errorField"></span>
                                    </div>
                                
                                    <!-- Empresa Cliente Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('empresa_cliente', 'Empresa:') !!}
                                        <input type="text" name="empresa_cliente" id="empresa_cliente" class="form-control" required minlength="3">
                                        <span id="eEmpresa" class="errorField"></span>
                                    </div>
                                    <!-- Telefono Cliente Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('telefono_cliente', 'Telefono:') !!}
                                        <input type="tel" name="telefono_cliente" id="telefono_cliente" class="form-control" required minlength="10" maxlength="15">
                                        <span id="eTelefono" class="errorField"></span>
                                    </div>

                                    <!-- Email Cliente Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('email_cliente', 'Correo electrónico:') !!}
                                        <input type="email" name="email_cliente" id="email_cliente" class="form-control" required>
                                        <span id="eCorreo" class="errorField"></span>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                
                                <div class="modal-footer" style="text-align:center">
                                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                                    <button type="button" id="btnEnvio" class="botonEnviar">Generar cotización</button>
                                    <!-- {!! Form::submit('Generar cotización', ['class' => 'botonEnviar','id' => 'btnEnvio']) !!} -->
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
        <!-- aqui termina el chido -->
                </div>
            </div>
        </main>
        
    </div>
</div>
@include('front.footer')
</body>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
<script src="{!! asset('assets/scripts/extras.1.1.0.min.js') !!}"></script>
<script src="{!! asset('assets/scripts/shards-dashboards.1.1.0.min.js') !!}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.2.0/knockout-min.js"></script>

<script>

    var d = new Date();
    
    var anio_actual = d.getFullYear();
    
    for(var x = 0; x < 30; x++)
    {
        $('#anio').append('<option value="'+ (anio_actual - x) + '">'+ (anio_actual - x) +'</option>');
    }
    
</script>

<script>    
    $("#valor_factura_con_iva").on({
    "focus": function(event) {
        $(event.target).select();
    },
    "keyup": function(event) {
        $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
    });

    $("#poliza_seguro_anual2").on({
    "focus": function(event) {
        $(event.target).select();
    },
    "keyup": function(event) {
        $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
    });
 
    function evaluarContenido(cadenaIngresada){
        var evaluarCadena = cadenaIngresada.split("");
        if(evaluarCadena[0] != '$'){
            $("#valor_factura_con_iva").val("$"+cadenaIngresada);
        }
    }

    function darFormato(input)
    {
        return input.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    }
    
    function evaluarContenido2(cadenaIngresada){
        var evaluarCadena = cadenaIngresada.split("");
        if(evaluarCadena[0] != '$'){
            $("#valor_factura_sin_iva2").val("$"+cadenaIngresada);
        }
    }

    function evaluarContenido3(cadenaIngresada){
        var evaluarCadena = cadenaIngresada.split("");
        if(evaluarCadena[0] != '$'){
            $("#poliza_seguro_anual2").val("$"+cadenaIngresada);
        }
    }
    
</script>

<script>
    $('#popup').click(function() {
        // CAMPO DE SELECT DE BIENES
        function validaBien(){
            if ($('#selectBienes').val() === null) {
                $("#eBien").html('Es necesario seleccionar un bien');
                return false;
            } 
            else {
                $("#eBien").html('');                            
                return true;
            }
        }

        function validaMarca(){
            // CAMPO DE MARCA 
            if ($('#marca').val() === '') {
                $("#eMarca").html('El campo es necesario');
                return false;
            } 
            else{
                if($("#marca").val().length < 3){
                    $("#eMarca").html('El campo debe tener como mínimo 3 caracteres');
                    return false;
                }
                else{
                $("#eMarca").html('');                            
                return true;
                }
            }
        }

        function valdiaModelo(){
            // CAMPO DE MODELO
            if ($('#modelo').val() === '') {
                $("#eModelo").html('El campo es necesario');
                return false;
            } 
            else{
                if($("#modelo").val().length < 3){
                    $("#eModelo").html('El campo debe tener como mínimo 3 caracteres');
                    return false;
                }
                else{
                $("#eModelo").html('');                            
                return true;
                }
            }
        }
        
        function validaAnio(){
            // CAMPO DE AÑO
            if ($('#anio').val() === null) {
                $("#eAnio").html('El campo es necesario');
                return false;
            } 
            else {
                $("#eAnio").html('');                    
                return true;        
            }
        }

        function validaFacturaIva(){
            // CAMPO DE VALOR A FACTURAR CON IVA
            if ($('#valor_factura_con_iva').val() == '$') {
                $("#eValorFacturaConIva").html('El campo es necesario');
                return false;
            } 
            else {
                $("#eValorFacturaConIva").html('');                            
                return true;
            }
        }
        
        function validaSeguro(){
            if ($('#poliza_seguro_anual2').val() == '$') {
                $("#ePoliza").html('Coloque 0 si no aplica');
                return false;
            }
            else{
                $("#ePoliza").html('');        
                return true;            
            }
        }

        if(validaBien() == false || validaMarca() == false || valdiaModelo() == false || validaAnio() == false || validaFacturaIva() == false || validaSeguro() == false)
        {
            $('#popup').removeAttr('data-target');
            $('html,body').animate({
                scrollTop: $("#tope").offset().top
            }, 500);
        }
        else{
            $('#popup').attr('data-target', '#exampleModalCenter');
        }
    });

    function caracteresCorreoValido(email){            
        var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

        if (caract.test(email) == false){
            return false;
        }
        else{
            return true;
        }
    }

    // DATOS DEL CLIENTE
    $('#btnEnvio').click(function() {
        // NOMBRE 
        function validoNombre(){
            if ($('#nombre_cliente').val() === '') {
            $("#eNombreCliente").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#nombre_cliente").val().length < 3) { 
                    $("#eNombreCliente").html('El campo debe tener como mínimo 3 caracteres');                
                    return false;
                }
                {
                    $("#eNombreCliente").html('');
                    return true;
                }
            }
        }
        function validoApePat(){
            if ($('#apellido_paterno_cliente').val() === '') {
            $("#eApePat").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#apellido_paterno_cliente").val().length < 4) { 
                    $("#eApePat").html('El campo debe tener como mínimo 4 caracteres');
                    return false;
                }
                else{
                    $("#eApePat").html('');
                    return true;
                }
            }
        }
        function validoApeMat(){
            if ($('#apellido_materno_cliente').val() === '') {
            $("#eApeMat").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#apellido_materno_cliente").val().length < 4) { 
                    $("#eApeMat").html('El campo debe tener como mínimo 4 caracteres');
                    return false;
                }
                else{
                    $("#eApeMat").html('');
                    return true;
                }
            }
        }
        function validoEmpresa(){
            if ($('#empresa_cliente').val() === '') {
            $("#eEmpresa").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#empresa_cliente").val().length < 3) { 
                    $("#eEmpresa").html('El campo debe tener como mínimo 3 caracteres');
                    return false;
                }
                else{
                    $("#eEmpresa").html('');
                    return true;
                }
            }
        }
        function validoTelefono(){
            if ($('#telefono_cliente').val() === '') {
            $("#eTelefono").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#telefono_cliente").val().length < 10) { 
                    $("#eTelefono").html('El telefóno debe tener como minimo 10 caracteres');   
                    return false;             
                }
                else{
                $("#eTelefono").html('');
                return true;
                }
            }
        }
        function validoCorreo(){
            if ($('#email_cliente').val() === '') {
            $("#eCorreo").html('El campo es necesario');
            return false;
            } 
            else {
                if(caracteresCorreoValido($('#email_cliente').val()))
                {
                    $("#eCorreo").html('');
                    return true;
                }
                else
                {
                    $("#eCorreo").html('El correo electrónico no es valido');
                    return false;
                }
                
            }
        }
       
       if(validoNombre() == false || validoApePat() == false || validoApeMat() == false || validoEmpresa() == false || validoTelefono() == false || validoCorreo() == false) 
       {
        
       }
       else{
        $("#formularioXD").submit();
       }
        
    });
</script>
<script>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
</script>
<script>

    // SLIDER DE PLAZO inicial
    
    var valoresPlazosTemporal = {!! $valoresPlazo !!};
    var valoresPlazos = [];
    for(var x = 0; x < valoresPlazosTemporal.length;x++)
    {
        valoresPlazos[x] = valoresPlazosTemporal[x].valor;
    }
    $(function() {
        $( "#sliderPlazo" ).slider({
            value: 0, 
            min: 0, 
            max: valoresPlazos.length-1, 
            range: "max",
            slide: function( event, ui ) {
            $( "#valoresPlazo" ).text(valoresPlazos[ui.value] + " meses" ); 
            $( "#valorPlazoOculto" ).text(valoresPlazos[ui.value]);
            $("#pago_mensual_fijo").val(pagoMensualFijo()); 
            $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
            $("#renta_en_deposito").val(rentaDeposito());
            $("#total_pago_inicial").val(totalPagoInicial()); 
            $("#valor_residual_sin_iva").val(valorResidualSinIva());
            $("#total_por_pagar").val(totalPorPagar());
            $("#plazo").val(valoresPlazos[ui.value]);
            $("#valor_residual_calculo").val(valorResidual());
            actualizarValores();
            }
        });
        $( "#valoresPlazo" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")] + " meses" );
        $( "#valorPlazoOculto" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
        $( "#plazo" ).val(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
        $( "#valor_residual_calculo" ).val(valorResidual()[$( "#sliderPlazo" ).slider( "value")]);
        $("#valor_residual_calculo").val(valorResidual());
        
    });

    //  funcion para crear el slider de plazos
    function crearSliderPlazo() {
        $(function() {
            $( "#sliderPlazo" ).slider({
                value: 0, 
                min: 0, 
                max: valoresPlazos.length-1, 
                range: "max",
                slide: function( event, ui ) {
                $( "#valoresPlazo" ).text(valoresPlazos[ui.value] + " meses" ); 
                $( "#valorPlazoOculto" ).text(valoresPlazos[ui.value]);
                $("#pago_mensual_fijo").val(pagoMensualFijo()); 
                $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
                $("#renta_en_deposito").val(rentaDeposito()); 
                $("#total_pago_inicial").val(totalPagoInicial()); 
                $("#valor_residual_sin_iva").val(valorResidualSinIva());
                $("#total_por_pagar").val(totalPorPagar());
                $("#plazo").val(valoresPlazos[ui.value]);
                $("#valor_residual_calculo").val(valorResidual());
                actualizarValores();

                }
            });
            $( "#valoresPlazo" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")] + " meses" );
            $( "#valorPlazoOculto" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
            $( "#plazo" ).val(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
            $("#valor_residual_calculo").val(valorResidual());
            
        });
    }

    //SLIDER DE PAGO INCIAL
    // función para saber en que opción del select estoy
    function tipoBien(dato)
    {
        if(dato == "Automóviles")
        {
            // var valoresPagoInitGeneral = [3, 5, 10, 15, 20, 25, 30];
            var valoresPagoInitGeneral = {!! $valoresPagoInicialGeneral !!};
            var valoresPagoInit = [];
            for(var x = 0; x < valoresPagoInitGeneral.length;x++)
            {
                valoresPagoInit[x] = valoresPagoInitGeneral[x].valor;
            }
            var valorPagoInicial = valoresPagoInit[0];
            crearSliderPagoInit(valoresPagoInit,valorPagoInicial);
            crearSliderPlazo();
            var valorFactura =  $("#valor_factura_sin_iva").val();
            var vpi = (valorPagoInicial / 100);
            var result = (vpi * valorFactura);
            $("#pago_inicial").val(result);
            $("#valor_residual_calculo").val(valorResidual());
            
        }
        else
        {
            // var valoresPagoInit = [20, 25, 30];
            var valoresPagoInitParticulares = {!! $valoresPagoInicialNoParticulares !!};
            
            var valoresPagoInit = [];
            for(var x = 0; x < valoresPagoInitParticulares.length;x++)
            {
                valoresPagoInit[x] = valoresPagoInitParticulares[x].valor;
            }
            var valorPagoInicial = valoresPagoInit[0];
            crearSliderPagoInit(valoresPagoInit,valorPagoInicial);   
            crearSliderPlazo();  
            var valorFactura =  $("#valor_factura_sin_iva").val();    
            var vpi = (valorPagoInicial / 100);
            var result = (vpi * valorFactura);
            $("#pago_inicial").val(result);
            $("#valor_residual_calculo").val(valorResidual());
            
        }

        if(dato == "Automóviles" || dato == "Flotillas" || dato == "Camiones")
        {
            // $("#localizadorValidado").show();
            $('#formLocalizador').prop('checked',true);
        }
        else
        {
            // $("#localizadorValidado").hide();
            $('#formLocalizador').prop('checked',false);

        }
    }

    // funcion para iniciar el slider de pago inicial la primera vez.
    $(function() {
        // var valoresPagoInit = [20, 25, 30];
        var valoresPagoInitParticulares = {!! $valoresPagoInicialNoParticulares !!};
        
        var valoresPagoInit = [];
        for(var x = 0; x < valoresPagoInitParticulares.length;x++)
        {
            valoresPagoInit[x] = valoresPagoInitParticulares[x].valor;
        }
        var valorPagoInicial = valoresPagoInit[0];
        $( "#sliderPagoInicial" ).slider({
            value: 0, 
            min: 0, 
            max: valoresPagoInit.length-1, 
            range: "max",
            slide: function( event, ui ) {
            $( "#valoresPagoInicial" ).text(valoresPagoInit[ui.value] + "%" ); 
            $("#valorPagoInicialOculto").text(valoresPagoInit[ui.value]);
            valorPagoInicial = valoresPagoInit[ui.value];
            $("#pago_inicial").val(calPagoInit());
            $("#pago_mensual_fijo").val(pagoMensualFijo());
            $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
            $("#subtotal").val(subtotal());
            $("#gastos_de_apertura").val(gastosDeApertura());
            $("#iva").val(iva());
            $("#renta_en_deposito").val(rentaDeposito()); 
            $("#total_pago_inicial").val(totalPagoInicial()); 
            $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
            $("#total_por_pagar").val(totalPorPagar());
            $("#porcentaje_pago_inicial").val(valoresPagoInit[ui.value]);
            actualizarValores();
            }
        });
        $( "#valoresPagoInicial" ).text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")] + "%" );
        $("#valorPagoInicialOculto").text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
        $("#porcentaje_pago_inicial").val(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
    });

    // funcion para crear el slider de pago inicial
    function crearSliderPagoInit(valoresPagoInit,valorPagoInicial){
        $(function() {
            $( "#sliderPagoInicial" ).slider({
                value: 0, 
                min: 0, 
                max: valoresPagoInit.length-1, 
                range: "max",
                slide: function( event, ui ) {
                $( "#valoresPagoInicial" ).text(valoresPagoInit[ui.value] + "%" );
                $("#valorPagoInicialOculto").text(valoresPagoInit[ui.value]);
                valorPagoInicial = valoresPagoInit[ui.value];
                $("#pago_inicial").val(calPagoInit());
                $("#pago_mensual_fijo").val(pagoMensualFijo());
                $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
                $("#subtotal").val(subtotal());
                $("#gastos_de_apertura").val(gastosDeApertura());
                $("#iva").val(iva());
                $("#renta_en_deposito").val(rentaDeposito()); 
                $("#total_pago_inicial").val(totalPagoInicial()); 
                $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
                $("#total_por_pagar").val(totalPorPagar());
                $("#porcentaje_pago_inicial").val(valoresPagoInit[ui.value]);
                actualizarValores();
                }
            });
            $( "#valoresPagoInicial" ).text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")] + "%" );
            $( "#valorPagoInicialOculto").text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
            $("#porcentaje_pago_inicial").val(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);        
        });
    }

    // funcion para ejecutar el calculo al ingresar datos en el valor de factura sin iva
    $("#valor_factura_sin_iva").keyup(function(event){       
        $("#pago_inicial").val(calPagoInit());
        $("#subtotal").val(subtotal());
        $("#gastos_de_apertura").val(gastosDeApertura());
        $("#iva").val(iva());
        $("#pago_mensual_fijo").val(pagoMensualFijo());
        $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#total_por_pagar").val(totalPorPagar());
        actualizarValores();
    }); 

    // funcion para pasarle el valor a factura sin iva
    $("#valor_factura_con_iva").keyup(function(event){       
        // var valor_factura_sin_iva = ($("#valor_factura_con_iva").val() / 1.16);

        var vfsiString = $("#valor_factura_con_iva").val().toString();

        evaluarContenido(vfsiString);
             
        vfsiString = vfsiString.replace(',','');
        vfsiString = vfsiString.replace(',','');
        vfsiString = vfsiString.replace(',','');
        vfsiString = vfsiString.replace(',','');

        // vfsiString = parseInt(vfsiString); 
        
        
        vfsiString = parseFloat(vfsiString);        

        var valor_factura_sin_iva = (vfsiString / 1.16);
        
        $("#valor_factura_sin_iva").val(valor_factura_sin_iva.toFixed(2));

        $("#valor_factura_sin_iva2").val(valor_factura_sin_iva.toFixed(2));

        var aux = darFormato($("#valor_factura_sin_iva2").val());

        

        evaluarContenido2(aux);

        $("#pago_inicial").val(calPagoInit());
        $("#subtotal").val(subtotal());
        $("#gastos_de_apertura").val(gastosDeApertura());
        $("#iva").val(iva());
        $("#pago_mensual_fijo").val(pagoMensualFijo());
        $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#total_por_pagar").val(totalPorPagar());
        

        var comision = {!! $comision->valor !!} / 100;
        $("#porcentaje_comision").val(comision);

        var vr_adicional = {!! $valorResidualAdicional->valor !!}        
        
        $("#porcentaje_adicional_vr").val(vr_adicional);
        
        var tasa = ({!! $tasa->valor !!});
        $("#porcentaje_tasa").val(tasa);
        $("#iva").val(iva());
    }); 

    $("#poliza_seguro_anual").keyup(function(event){
        actualizarValores();
    });

    $("#poliza_seguro_anual2").keyup(function(event){
        evaluarContenido3($("#poliza_seguro_anual2").val());

        var aux_poliza = $("#poliza_seguro_anual2").val().toString();

        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace('$','');

        aux_poliza = parseFloat(aux_poliza);        

        $("#poliza_seguro_anual").val(aux_poliza.toFixed(2));
        actualizarValores();
        actualizarValores();
    });

    // función para calcular el pago inicial 
    function calPagoInit(){
        var valorFactura =  $("#valor_factura_sin_iva").val();    
        var valorPagoInicial = $("#valorPagoInicialOculto").text();
        var vpi = (valorPagoInicial / 100);
        var result = (vpi * valorFactura);
        return result.toFixed(2);
    }

    // funcion para sacar el subtototal
    function subtotal() {
        var valorFactura = $("#valor_factura_sin_iva").val();
        var valorPagoInicial = $("#valorPagoInicialOculto").text();   
        var pago_inicial = (valorPagoInicial / 100) * valorFactura;   
        var comision = {!! $comision->valor !!} / 100;
        var importe = valorFactura * comision;   
        var localizador = localizadorCostoMensual();
        
        if( $('#formLocalizador').prop('checked') ) {         
        var subtotal = importe + pago_inicial + 650;
        }
        else
        {
            var subtotal = importe + pago_inicial;
        }
       
        return subtotal.toFixed(2);
    }

    

    // funcion para sacar el pago mensual fijo
    function pagoMensualFijo() {
        var valorFactura = $("#valor_factura_sin_iva").val();
        var pago_inicial = ($("#valorPagoInicialOculto").text() /100);
        var importe_monto_financiar = (pago_inicial * valorFactura );
        var monto_financiar = valorFactura - importe_monto_financiar;
        var plazo = $("#valorPlazoOculto").text();
        
        var valor_residual = valorResidual();
        getValorResidual(valor_residual);
        var importe_valor_residual = ((valor_residual /100) * valorFactura);
        var tasa = ({!! $tasa->valor !!} / 100);
        var pago = PMT((tasa/12),plazo,(-(monto_financiar)),importe_valor_residual);
                
        var localizador = localizadorCostoMensual();
        var seguro = polizaSeguroAnual();
        
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            var iva = ((pago + localizador + seguro) * .16);
            var result = pago + iva + localizador + seguro;
        }
        else{
            var iva = ((pago + localizador) * .16);
            var result = pago + iva + localizador;
        }
        
        return result.toFixed(2);
    }


    function pagoMensualFijoSinIva() {
        var valorFactura = $("#valor_factura_sin_iva").val();
        var pago_inicial = ($("#valorPagoInicialOculto").text() /100);
        var importe_monto_financiar = (pago_inicial * valorFactura );
        var monto_financiar = valorFactura - importe_monto_financiar;
        var plazo = $("#valorPlazoOculto").text();

        var valor_residual = valorResidual();
        getValorResidual(valor_residual);
        var importe_valor_residual = ((valor_residual /100) * valorFactura);
        var tasa = ({!! $tasa->valor !!} / 100);        
        var pago = PMT((tasa/12),plazo,(-(monto_financiar)),importe_valor_residual);
        
        var localizador = localizadorCostoMensual();
        var seguro = polizaSeguroAnual();
                 
        var iva = (pago * .16);
        
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            var result = pago + localizador + seguro;
        }
        else{
            var result = pago + localizador;
        }
        

        return result.toFixed(2);
    }

    var valor_localizador = {!! $localizador->valor !!}
    valor_localizador = parseInt(valor_localizador);
    function localizadorCostoMensual()
    {   
        var localizador = 0;

        if( $('#formLocalizador').prop('checked') ) {
            localizador = valor_localizador * 1.15;            
        }
        else
        {
            localizador = 0;
        }
        return localizador;
    }

    function actualizarValores()
    {
        // var valor_factura_sin_iva = ($("#valor_factura_con_iva").val() / 1.16);

        var vfsiString = $("#valor_factura_con_iva").val().toString();
        
        vfsiString = vfsiString.replace(',','');
        vfsiString = vfsiString.replace(',','');
        vfsiString = vfsiString.replace(',','');
        vfsiString = vfsiString.replace(',','');

        vfsiString = vfsiString.replace('$','');
        
        
        
        vfsiString = parseInt(vfsiString); 
        
        var valor_factura_sin_iva = (vfsiString / 1.16);

        $("#valor_factura_sin_iva").val(valor_factura_sin_iva.toFixed(2));
        
        var aux = darFormato($("#valor_factura_sin_iva").val());

        evaluarContenido2(aux);  

        $("#pago_inicial").val(calPagoInit());
        $("#subtotal").val(subtotal());
        $("#gastos_de_apertura").val(gastosDeApertura());
        $("#iva").val(iva());
        $("#pago_mensual_fijo").val(pagoMensualFijo());
        $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#total_por_pagar").val(totalPorPagar());

        var comision = {!! $comision->valor !!} / 100;
        $("#porcentaje_comision").val(comision);

        var vr_adicional = {!! $valorResidualAdicional->valor !!}        
        
        $("#porcentaje_adicional_vr").val(vr_adicional);
        
        var tasa = ({!! $tasa->valor !!});
        $("#porcentaje_tasa").val(tasa);
        $("#iva").val(iva());
    }

    // funcion para sacar el valor residual
    function getValorResidual(valor_residual) {
        $("#valor_residual_calculo").val(valor_residual);
    }

    // funcion replica de PMT de excel
    function PMT(rate, nperiod, pv, fv, type) 
    { 
        if (!fv) fv = 0; if (!type) type = 0; if (rate == 0) return -(pv + fv)/nperiod; var pvif = Math.pow(1 + rate, nperiod); var pmt = rate / (pvif - 1) * -(pv * pvif + fv); if (type == 1) { pmt /= (1 + rate); }; return pmt; 
    }

    function iva(){
        var subtotal = $("#subtotal").val();
        var iva = (subtotal * .16);

        var primer_renta = rentaDeposito(); 
        
        var iva_primer_renta = (primer_renta * .16);

        var total = (iva_primer_renta +iva);
        
        return iva.toFixed(2);
    }

    function rentaDeposito() {
        var pagoMensualFijo = $("#pago_mensual_fijo").val();
        var rentaDeposito = (pagoMensualFijo / 1.16);
        
        return rentaDeposito.toFixed(2);
    }

    function totalPagoInicial(){
        var subtotal = $("#subtotal").val();
        var iva = $("#iva").val();
        var rentaDeposito = $("#renta_en_deposito").val();
        
        var total = parseFloat(subtotal) + parseFloat(iva) + parseFloat(rentaDeposito);
        
        return total.toFixed(2);
    }

    var vr_adicional = {!! $valorResidualAdicional->valor !!}
    vr_adicional = parseInt(vr_adicional);
    
    function valorResidualSinIva() {
        var valor_residual = valorResidual();
        var valor_sin_iva = valor_residual + vr_adicional;
        var valorFactura = $("#valor_factura_sin_iva").val();
        var final = (valor_sin_iva/100) * valorFactura
        return final.toFixed(2);
    }


    var array_autos = {!! $valorResidualAutos !!};
    var array_flotillas = {!! $valorResidualFlotillas !!};
    var array_camiones = {!! $valorResidualCamiones !!};
    var array_maquinaria = {!! $valorResidualMaquinaria !!};
    var array_electronica = {!! $valorResidualElectronica !!};
    
    function valorResidual() {
        var valor_residual = 0;
        var tipo_bien = $("#selectBienes").val();
        var plazo = $("#valorPlazoOculto").text();

        if(tipo_bien == "Automóviles")
        {
            array_autos.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });
            
            // if(plazo == 12)
            // {
            //     valor_residual = 50;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 40;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 30;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 20;
            // }
        }
        else if(tipo_bien == "Flotillas")
        {
            array_flotillas.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 40;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 30;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 10;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 5;
            // }
        }
        else if(tipo_bien == "Camiones")
        {

            array_camiones.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 35;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 25;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 15;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 5;
            // }
        }
        else if(tipo_bien == "Maquinaria, Mobiliario y Equipo")
        {

            array_maquinaria.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 40;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 30;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 10;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 0;
            // }
        }
        else
        {

            array_electronica.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 20;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 10;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 0;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 0;
            // }
        }
        return valor_residual;
    } 

    function totalPorPagar(){
        var total_pago_inicial = $("#total_pago_inicial").val();
        var plazo = $( "#valorPlazoOculto" ).text();
        var pago_mensual_fijo = $("#pago_mensual_fijo").val(); 
        var valor_residual_sin_iva = $("#valor_residual_sin_iva").val();
        var total_a_pagar = ((parseFloat(total_pago_inicial) + (plazo - 1) * parseFloat(pago_mensual_fijo) + (parseFloat(pago_mensual_fijo) / 1.16 * .16) + parseFloat(valor_residual_sin_iva) * 1.16)); 
        
        return total_a_pagar.toFixed(2);
    }

    function gastosDeApertura()
    {
        var valorFactura = $("#valor_factura_sin_iva").val();   
        var comision = {!! $comision->valor !!} / 100;
        var importe = valorFactura * comision;
        
        if( $('#formLocalizador').prop('checked') ) {
            var localizador = 650;
        }
        else{
            localizador = 0;
        }
        var gastosApertura = localizador + importe;
        
        return gastosApertura.toFixed(2);
    }

    function polizaSeguroAnual()
    {
        var monto_poliza = $('#poliza_seguro_anual').val();
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            monto_poliza = monto_poliza * 1.1;
        }
        else{
            monto_poliza = monto_poliza;
        }        
        var seguro = 0;
        var tasa = ({!! $tasa->valor !!} / 100);
        
        
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            seguro = PMT(tasa/12,12,-monto_poliza,0,0);                                    
        }
        else
        {
            if(monto_poliza == 0)
            {
                seguro = 0;
            }
            else{
                seguro = (monto_poliza / 1.16) + 800;
            }            
            
        }

        return parseFloat(seguro.toFixed(2));
    }
</script>

</html>