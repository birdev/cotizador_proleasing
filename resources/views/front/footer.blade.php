<style>
.c-about{
    color: #a7a9ac;
    font-size: 15px;
    padding: 10px 0;
    font-weight:300;
}

.c-contact{
    color: #a7a9ac;
    font-size: 15px;
    padding: 20px 0;
    font-weight:300;
}

.c-socials{
    list-style: none;
    padding: 0;
    margin: 0;
}

.c-socials > li{
    display: inline-block;
    padding-right: 5px;
}

.link:hover{
    color:#ec7d00;
    text-decoration: none;
}

.inputCustom{
    color: #fff;
    font-size: 17px;
    font-weight: 300;
    border-radius: 0;
    box-shadow: none;
    border-color: #fff;
    background: transparent;
    padding: 8px 16px;
    margin-bottom: 20px;
    border-radius: 0px;
    border-radius: 0px !important;
}

.btn-lg{
    padding: 12px 26px 12px 26px;
    color: #FFFFFF;
    background: none;
    border-color: #FFFFFF;
    border-radius: 0px !important;

}

</style>
<footer style="background: #333333; color: white; height:100%" class="main-footer">
    <div class="c-footer">
        <div style="margin: 10px 20%;">
            <div class="row">
                <div class="col-md-6 c-footer-4-p-right" style="padding: 20px 0px">
                    <div class="c-content-title-1">
                        <h3 style="color:white;font-weight: 600;font-size: 30px;">CONTACTO
                        </h3>
                    </div>
                    <p class="c-about">PUEBLA <br>31 oriente No. 616 local E (Plaza Ultra) Col. Anzures <br>Tel (222) 2 37 03 34 (222) 1 87 50 68 </p>
                    <p class="c-contact"> CDMX <br> Periférico Sur #3449 6º piso, Col. San Jerónimo Lídice<br> C.P. 10200, Del. Magdalena Contreras
                    <br>Tel (55) 53 58 07 54 (55) 70 30 05 65</p><!--General Mariano Artista No. 171 Col. Argentina poniente <br>Delegación Miguel Hidalgo -->
                    <ul class="c-socials">
                        <li>
                            <a href="#">
                                <img src="http://www.pro-leasing.mx/images/home-ico-2.png">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="http://www.pro-leasing.mx/images/home-ico-3.png">
                            </a>
                        </li>
                    </ul>
                    <p style="margin: 20px 0px;"><a href="http://www.pro-leasing.mx/aviso-privacidad.php" class="c-font-white link">Aviso de privacidad</a></p>
                </div>
                <!-- <div class="col-md-6 c-footer-4-p-left">
                    <div class="c-feedback">
                        <form action="" name="formContacto" id="formContacto" method="post">
                            <input class="inputCustom form-control" type="hidden" name="cmd" value="send">
                            <input class="inputCustom form-control" type="text" placeholder="Nombre" class="form-control" name="entity[Nombre]" value="" id="Nombre" required="">
                            <input class="inputCustom form-control" type="email" placeholder="Email" class="form-control" name="entity[Email]" value="" id="Email" required="">
                            <textarea class="inputCustom form-control" rows="8" name="Mensaje" id="Mensaje" placeholder="Comentarios ..." class="form-control"></textarea>
                            <button type="submit" onClick="this.disabled=true" class="btn c-btn-white c-btn-border-2x c-btn-uppercase btn-lg c-btn-bold c-btn-square">Enviar</button>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</footer>