<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Información</span>
        <h3 class="page-title">Información completa del usuario</h3>
        </div>
    </div>
</div>
<div class="form-group col-sm-12 row">
<!-- Id Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $user->id !!}</p>
</div> -->

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{!! $user->name !!} {!! $user->apellido_paterno !!} {!! $user->apellido_materno !!}</p>
</div>

<!-- Apellido Paterno Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('apellido_paterno', 'Apellido Paterno:') !!}
    <p>{!! $user->apellido_paterno !!}</p>
</div> -->

<!-- Apellido Materno Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('apellido_materno', 'Apellido Materno:') !!}
    <p>{!! $user->apellido_materno !!}</p>
</div> -->

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Correo electrónico:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- Password Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('password', 'Contraseña:') !!}    
    <p>{!! $user->password !!}</p>
</div> -->

<!-- Tipo User Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tipo_user', 'Tipo usuario:') !!}
    <p>{!! $user->tipo_user !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{!! $user->created_at !!}</p>
</div>

<!-- Updated At Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $user->updated_at !!}</p>
</div> -->

<!-- Deleted At Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $user->deleted_at !!}</p>
</div> -->

<!-- Remember Token Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    <p>{!! $user->remember_token !!}</p>
</div> -->

</div>