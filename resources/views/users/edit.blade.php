@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h5>
            <!-- Editar usuario -->
        </h5>
   </section>
   <div class="content" style="width: 100%">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                    <div class="main-content-container container-fluid px-4"> 
                   {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

                        @include('users.fields')

                   {!! Form::close() !!}
                    </div>
               </div>
           </div>
       </div>
   </div>
@endsection