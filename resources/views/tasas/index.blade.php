@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Tasas</h1>
        <h1 class="pull-right">
           <!-- <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('tasas.create') !!}">Agregar nueva tasa</a> -->
           <br>
        </h1>
    </section>
    <div class="form-group col-sm-12 row">
        @include('flash::message')        
        <div class="form-group col-sm-12">  
            @include('tasas.table')
        </div>
    
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tasas-table').DataTable({
            responsive: true,
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json' //Ubicacion del archivo con el json del idioma.
            }
        
        });
    });
</script>
@endsection