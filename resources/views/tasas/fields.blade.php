<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Datos de la tasa</span>
        <h3 class="page-title">Llene el formulario con los datos solicitados</h3>
        </div>
    </div>
</div>
<div class="form-group col-sm-6 row">
    <!-- Nombre Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('tasas.index') !!}" class="btn btn-secondary">Cancelar</a>
    </div>
</div>