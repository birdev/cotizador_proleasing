<table class="table table-responsive-sm" id="tasas-table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tasas as $tasa)
        <tr>
            <td>{!! $tasa->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['tasas.destroy', $tasa->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tasas.edit', [$tasa->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-edit"></i></a>
                    <!-- {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro de eliminar esta tasa?')"]) !!} -->
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>