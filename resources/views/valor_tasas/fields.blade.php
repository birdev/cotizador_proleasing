<?php
$tasas = \App\Models\Tasa::all();
$bienes = \App\Models\Bien::all();
$plazos = \App\Models\ValorTasa::where('tasa_id','=',2)->get();

$bandera = 0;
$bandera2 = 0;
if($valorTasa != "NULL")
{
    $section_array = explode(",", $valorTasa->bien_section);
    $plazo_array = explode(",", $valorTasa->plazo_vr);
}
else{
    
}
?>
<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Valor de la tasa</span>
        <h3 class="page-title">Llene el formulario con los datos solicitados</h3>
        </div>
    </div>
</div>
<div class="form-group col-sm-12 row">
    <!-- Tasa Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tasa_id', 'Tasa:') !!}
        <select class="form-control" id="selectTasas" name="tasa" onchange="tipoBien(this.value)">
            <option value="" disabled selected>Selecciona una tasa</option>
            @if($valorTasa != "NULL")    
                @foreach($tasas as $tasa)     
                        <option value='{!! $tasa->id !!}' @if($valorTasa->tasa_id == $tasa->id) selected @endif disabled>{!! $tasa->nombre !!}</option>
                @endforeach
            @else
                @foreach($tasas as $tasa)     
                    @if($tasa->id == 3 || $tasa->id == 4 || $tasa->id == 6 || $tasa->id == 7)
                    @else
                    <option value='{!! $tasa->id !!}'>{!! $tasa->nombre !!}</option>
                    @endif
                @endforeach
            @endif
        </select>  
    </div>
    @if($valorTasa != "NULL")
        @if($valorTasa->tasa_id != null)
        {!! Form::hidden('tasaIDupdate', $valorTasa->tasa_id, ['class' => 'form-control']) !!}
        @endif
    @endif
    <!-- Valor Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('valor', 'Valor:') !!}
        {!! Form::number('valor', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-12">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Selección de bienes</span>
            <h3 class="page-title">Marque los bienes a los cuales correspondera este valor</h3>
            </div>
        </div>
    </div>

    <!-- bien_section field -->
    <div class="form-group col-sm-12">
        <table class="table table-responsive-sm" id="bien-table">
            <thead>
                <tr>          
                    <th>Disponible para</th>
                    <th>Marcar</th>
                </tr>
            </thead>
            <tbody>
            @if($valorTasa != "NULL")    
                @foreach($bienes as $bien)
                    @foreach($section_array as $sa)
                        @if($bien->nombre == $sa)
                            <?php $bandera = 1;?>
                        @endif
                    @endforeach                    
                    @if($bandera == 1)
                        <tr>
                            <td>{!! $bien->nombre !!}</td>
                            <td><input type="checkbox"  name="bien_section[]" value="{!! $bien->nombre !!}" checked class="form-check-input"></td>        
                        </tr>
                    @else
                        <tr>
                            <td>{!! $bien->nombre !!}</td>
                            <td><input type="checkbox"  name="bien_section[]" value="{!! $bien->nombre !!}" class="form-check-input"></td>        
                        </tr>
                    @endif
                    <?php $bandera = 0;?>
                @endforeach
            @else
                @foreach($bienes as $bien)
                    <tr>
                        <td>{!! $bien->nombre !!}</td>
                        <td><input type="checkbox"  name="bien_section[]" value="{!! $bien->nombre !!}" class="form-check-input"></td>        
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    @if($valorTasa != "NULL" && $valorTasa->tasa_id == 5)   
    <div class="form-group col-sm-12" id="vr_campo">
    @else
    <div class="form-group col-sm-12" id="vr_campo" style="display:none">
    @endif
        <div class="form-group col-sm-12">
            <div class="page-header row no-gutters py-4">
                <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Selección de plazo para valor residual</span>
                <h3 class="page-title">Marque el plazo que corresponde al valor residual</h3>
                </div>
            </div>
        </div>

        <div class="form-group col-sm-12">
            <table class="table table-responsive" id="plazoResidual">
                <thead>
                    <tr>
                        <th>Disponible para</th>
                        <th>Marcar</th>
                    </tr>
                </thead>
                <tbody>
                @if($valorTasa != "NULL")
                    @foreach($plazos as $p)
                        @foreach($plazo_array as $pa)
                            @if($p->valor == $pa)
                            <?php $bandera2 = 1?>
                            @endif
                        @endforeach
                        @if($bandera2 == 1)
                        <tr>
                            <td>{!! $p->valor !!} meses</td>
                            <td><input type="checkbox"  name="plazo_vr[]" value="{!! $p->valor !!}" checked class="form-check-input"></td>
                        </tr>
                        @else
                        <tr>
                            <td>{!! $p->valor !!} meses</td>
                            <td><input type="checkbox"  name="plazo_vr[]" value="{!! $p->valor !!}" class="form-check-input"></td>
                        </tr>
                        @endif
                        <?php $bandera = 0;?>
                    @endforeach
                @else
                    @foreach($plazos as $p)
                        <tr>
                            <td>{!! $p->valor !!} meses</td>
                            <td><input type="checkbox"  name="plazo_vr[]" value="{!! $p->valor !!}" class="form-check-input"></td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('valorTasas.index') !!}" class="btn btn-secondary">Cancelar</a>
    </div>
</div> 