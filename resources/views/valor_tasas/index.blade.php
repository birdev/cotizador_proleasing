@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Ajuste de valores</h1>
        <h1 class="pull-right">
           <!-- <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('valorTasas.create') !!}">Agregar nuevo valor</a> -->
           <br>
        </h1>
    </section>
    <div class="form-group col-sm-12" style="style="padding-right: 0px; padding-left: 0px;"">
        @include('flash::message')
        <div class="form-group col-sm-12">            
            @include('valor_tasas.table')
        </div>
        
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#valorTasas-table').DataTable({
            responsive: true,
            "order": [[ 0, 'asc' ]],
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json' //Ubicacion del archivo con el json del idioma.
            }
        
        });
    });
</script>
@endsection