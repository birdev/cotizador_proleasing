<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Datos del bien</span>
        <h3 class="page-title">Información del bien</h3>
        </div>
    </div>
</div>
<!-- Id Field -->
<!-- <div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $bien->id !!}</p>
</div> -->

<div class="form-group col-sm-12 row">
    <!-- Nombre Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nombre', 'Nombre:') !!}
        <p>{!! $bien->nombre !!}</p>
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Created At Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('created_at', 'Creado el:') !!}
        <p>{!! $bien->created_at !!}</p>
    </div>
</div>
