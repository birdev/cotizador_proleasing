<table class="table table-responsive-sm" id="biens-table">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($biens as $bien)
        <tr>
            <td>{!! $bien->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['biens.destroy', $bien->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!-- <a href="{!! route('biens.show', [$bien->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-eye"></i></a> -->
                    <a href="{!! route('biens.edit', [$bien->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-edit"></i></a>
                    <!-- {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!} -->
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>