@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            <!-- Bien -->
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('biens.show_fields')
                    <a href="{!! route('biens.index') !!}" class="btn btn-primary">Regresar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
