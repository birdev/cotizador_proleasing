<li class="nav-item">
<a class="{{ Request::is('cotizacions*') ? 'nav-link active' : 'nav-link' }}" href="{!! route('cotizacions.index') !!}">
    <i class="material-icons">edit</i>
    <span>Cotizaciones</span>
</a>
</li>
<?php
$user = Auth::user();
?>
@if($user->tipo_user == "Administrador")

<li class="nav-item">
<a class="{{ Request::is('cotizaciones*') ? 'nav-link active' : 'nav-link' }}" href="{!! asset('cotizaciones/web') !!}">
    <i class="material-icons">laptop</i>
    <span>Cotzaciones Web</span>
</a>
</li>

<li class="nav-item">
<a class="{{ Request::is('users*') ? 'nav-link active' : 'nav-link' }} " href="{!! route('users.index') !!}">
    <i class="material-icons">vertical_split</i>
    <span>Usuarios</span>
</a>
</li>

<li class="nav-item">
<a class="{{ Request::is('valorTasas*') ? 'nav-link active' : 'nav-link' }}" href="{!! route('valorTasas.index') !!}">
    <i class="material-icons">table_chart</i>
    <span>Ajuste de tasas</span>
</a>
</li>       
@else
@endif