<!-- <aside class="main-sidebar" id="sidebar-wrapper"> -->

    <!-- sidebar: style can be found in sidebar.less -->
    <!-- <section class="sidebar"> -->

        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel">
            <div class="pull-left image">
                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                @if (Auth::guest())
                <p>InfyOm</p>
                @else
                    <p>{{ Auth::user()->name}}</p>
                @endif -->
                <!-- Status -->
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div> -->

        <!-- search form (Optional) -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
          <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
            </div>
        </form> -->
        <!-- Sidebar Menu -->

        <!-- <ul class="sidebar-menu">
            @include('layouts.menu')
        </ul> -->
        <!-- /.sidebar-menu -->
    <!-- </section> -->
    <!-- /.sidebar -->
<!-- </aside> -->

<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0" style="background-color: #212529 !important;">
            <a class="navbar-brand w-100 mr-0" href="http://www.pro-leasing.mx/" style="line-height: 25px;">
            <div class="d-table m-auto">
                <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 100%;" src="{!! asset('imagenes/logo.png') !!}" alt="Shards Dashboard">
                <!-- <span class="d-none d-md-inline ml-1">Proleasing</span> -->
            </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
            <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>
    <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
    <div class="input-group input-group-seamless ml-3">
        <div class="input-group-prepend">
        <div class="input-group-text">
            <!-- <i class="fas fa-search"></i> -->
        </div>
        </div>
        <input style="visibility:hidden" class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
    </form>
    <div class="nav-wrapper">
        <ul class="nav flex-column">
            @include('layouts.menu')
        </ul>
    </div>
</aside>