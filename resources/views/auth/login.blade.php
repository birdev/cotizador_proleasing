<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Proleasing - Administrador</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<style>
    @import url("//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css");
    .login-block{
    background: #333331;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to bottom, #bd7a21, #333331);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to bottom, #bd7a21, #333331); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    float:left;
    width:100%;
    padding : 100px 0px 200px 0px;
    }
    
    .banner-sec{background:url({!! asset('imagenes/fondo.jpg') !!})  no-repeat left bottom; background-size:cover; min-height:500px; border-radius: 0 10px 10px 0; padding:0;}
    .container{background:#fff; border-radius: 10px; box-shadow:15px 20px 0px rgba(0,0,0,0.1);}
    .carousel-inner{border-radius:0 10px 10px 0;}
    .carousel-caption{text-align:left; left:5%;}
    .login-sec{padding: 50px 30px; position:relative;}
    .login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
    .login-sec .copy-text i{color:#FEB58A;}
    .login-sec .copy-text a{color:#E36262;}
    .login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #bd7a21;}
    .login-sec h2:after{content:" "; width:100px; height:5px; background:#bd7a21; display:block; margin-top:20px; border-radius:3px; margin-left:auto;margin-right:auto}
    .btn-login{background: #333331; color:#fff; font-weight:600;}
    .banner-text{width:70%; position:absolute; bottom:40px; padding-left:20px;}
    .banner-text h2{color:#fff; font-weight:600;}
    .banner-text h2:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;}
    .banner-text p{color:#fff;}

    @media only screen and (max-width: 480px) {
        #logoGeneral{
            width: 300px !important;
            padding-bottom: 50px;
            margin-top: -50px;
        }
    }
</style>
<body class="hold-transition login-page">
    <section class="login-block">
        <div style="text-align:center">
            <a href="http://www.pro-leasing.mx/"><img id="logoGeneral" style="width:700px" src="./imagenes/logo-banner.png" alt=""></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 login-sec">
                    <h2 class="text-center">Inicio de sesión</h2>
                    <form class="login-form" method="post" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="exampleInputEmail1" class="text-uppercase">Correo electrónico</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ejemplo@proleasing.mx">
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="exampleInputPassword1" class="text-uppercase">Contraseña</label>
                            <input type="password" class="form-control" name="password" placeholder="*****">
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-check">
                            <button type="submit" class="btn btn-login float-right">Acceder</button>
                        </div>  
                    </form>
                </div>
                <div class="col-md-8 banner-sec">

                </div>
            </div>
        </div>
    </section>
</body>
</html>
