@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@endsection

@section('content')
    <section class="content-header">
    @if(count($cotizacions) != 0)
        <h1 class="pull-left">Cotizaciones</h1>
    @else
    <h1 style="font-family: 'Roboto', sans-serif;font-size: 36pt !important;font-weight: 800 !important;" class="pull-left">Crea tu primera cotización</h1>
    @endif
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('cotizacions.create') !!}">Nueva cotización</a>
        </h1>
    </section>
    <div class="form-group col-sm-12 row">
        @include('flash::message')
        <div class="form-group col-sm-12" style="padding-right: 0px;">
            @include('cotizacions.table')
        </div>
    </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@if(count($cotizacions) != 0)
    
@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#cotizacions-table').DataTable({
            responsive: true,
            "order": [[ 0, 'desc' ], [ 1, 'desc' ]],
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json' //Ubicacion del archivo con el json del idioma.
            }
        
        });
    });
</script>
@endsection
@endif