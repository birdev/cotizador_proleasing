<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Cotización {!! $cotizacion->id !!} </title>
    
  </head>
  <style>
    .clearfix:after {
    content: "";
    display: table;
    clear: both;
    }

    a {
    color: #5D6975;
    text-decoration: underline;
    }

    body {
    position: relative;
    /* width: 21cm;   */
    /* height: 29.7cm;  */
    margin: 0 auto; 
    color: #001028;
    background: #FFFFFF; 
    font-family: Arial, sans-serif; 
    font-size: 12px; 
    font-family: Arial;
    }

    header {
    padding: 0px 0;
    margin-bottom: 10px;
    }

    #logo {
    text-align: center;
    margin-bottom: 10px;
    background-color: black;
    }

    #logo img {
    width: 300px;
    padding: 10px;
    }

    h1 {
    border-top: 1px solid  #5D6975;
    border-bottom: 1px solid  #5D6975;
    color: #5D6975;
    font-size: 2.4em;
    line-height: 1.4em;
    font-weight: normal;
    text-align: center;
    margin: 0 0 20px 0;
    background: url(dimension.png);
    }

    #datos{
      margin: 0px 0px;      
    }

    #project {
    float: left;
    text-align: left;
    }

    #project span {
    color: #5D6975;
    text-align: right;
    width: 52px;
    margin-right: 10px;
    display: inline-block;
    font-size: 0.8em;    
    }

    #company {
      position: absolute;
      right: 0;
      width:60%;
      
    }

    #project div,
    #company div {
    white-space: nowrap;        
    }

    table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px;
    }

    table tr:nth-child(2n-1) td {
    background: #F5F5F5;
    }

    table th,
    table td {
    text-align: center;
    font-size: 14px;
    }

    table th {
    padding: 5px 20px;
    color: #5D6975;
    border-bottom: 1px solid #C1CED9;
    white-space: nowrap;        
    font-weight: normal;
    }

    table .service,
    table .desc {
    text-align: left;
    }
    
    table .total{
      text-align: right;
    }

    table td {
    padding: 10px 15px;
    text-align: right;
    }

    table td.service,
    table td.desc {
    vertical-align: top;
    }

    table td.unit,
    table td.qty,
    table td.total {
    font-size: 15px;
    }
    
    table td.subtotal{
      font-size: 13px;
      border-top: 1px solid #5D6975;
      border-bottom: 1px solid #5D6975;
      background-color: #ffa60096;      
    }

    table td.grand {
    border-top: 1px solid #5D6975;
    border-bottom: 1px solid #5D6975;
    }

    #notices .notice {
    color: #5D6975;
    font-size: 1em;
    }

    #monto{
      text-align:center;
      font-size: 20px;
      margin-top: -20px;
    }

    footer {
    color: #5D6975;
    width: 100%;
    height: 0px;
    position: absolute;
    bottom: 0;
    border-top: 1px solid #C1CED9;
    padding: 0px 0px;
    text-align: center;
    }
  </style>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{!! asset('imagenes/logo-banner2.png') !!}">
      </div>
      <h1>COTIZACIÓN DE ARRENDAMIENTO PURO</h1>
    <div id="datos">
        <div id="company">
            <div style="margin:-12px 0px 0px 0px;padding:5px;text-align:center"><strong>TELERENTA SA DE CV</strong></div>
            <div class="col-sm-12">
              <div style="padding: 0px 20px; float:left;">
                <div><strong>Sucursal CDMX</strong></div>
                <div>Periférico Sur #3449 6º piso,<br> Col. San Jerónimo Lídice<br/>Del. Magdalena Contreras, C.P. 10200 </div>
                <div> (55)5358-0754 </div>
                <div><a href="mailto:dir.mexico@pro-leasing.com.mx">dir.mexico@pro-leasing.com.mx</a></div>
              </div>
              <div style="padding: 0px 20px; float:right;">
                <div><strong>Sucursal Puebla</strong></div>
                <div>31 oriente No. 616 local E <br />(Plaza Ultra) Col. Anzures, Puebla</div>
                <div> (222) 2 37 03 34</div>
                <div><a href="mailto:dsveles@pro-leasing.com.mx">dsvelez@pro-leasing.com.mx</a></div>
              </div>
            </div>
        </div>
        <div id="project">
            <div><span>CLIENTE</span> {!! $cotizacion->nombre_cliente !!} {!! $cotizacion->apellido_paterno_cliente !!} {!! $cotizacion->apellido_materno_cliente !!} </div>
            <div><span>EMPRESA</span> {!! $cotizacion->empresa_cliente !!} </div>
            <div><span>EMAIL</span> <a href="mailto:{!! $cotizacion->email_cliente !!}">{!! $cotizacion->email_cliente !!}</a></div>
            <div><span>TELÉFONO</span> <a href="tel:{!! $cotizacion->telefono_cliente !!}">{!! $cotizacion->telefono_cliente !!}</a></div>
            <div><span>TIPO DE BIEN</span> {!! $cotizacion->bien->nombre !!} </div>
            <div><span>DETALLES</span> {!! $cotizacion->marca !!} {!! $cotizacion->modelo !!} {!! $cotizacion->anio !!} </div>
        </div>
    </div>
    </header>
    <!-- <div id="monto">
      <span>MONTO A FINANCIAR(SIN IVA)</span>
      <strong><p>$ {!! $cotizacion->valor_factura_sin_iva !!}</p></strong>
    </div> -->
    <main>
      <table>
        <thead>
          <tr>
          <th colspan="4" style="text-align:right">Concepto</th>
          <th style="text-align:right">Valor</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $precio_con_iva = ($cotizacion->valor_factura_sin_iva * 1.16);
          $gastos_apertura = ($cotizacion->gastos_de_apertura);
          $pago_mensual_fijo = number_format($cotizacion->pago_mensual_fijo,2,'.',',');
          
          $precio_con_iva = number_format($precio_con_iva,2,'.',',');          
          $pago_inicial = number_format($cotizacion->pago_inicial,2,'.',',');
          $gastos_apertura = number_format($gastos_apertura,2,'.',',');
          
          $iva = number_format($cotizacion->iva,2,'.',',');
          $renta_en_deposito = number_format($cotizacion->renta_en_deposito,2,'.',',');
          
          $valor_residual_sin_iva = number_format($cotizacion->valor_residual_sin_iva,2,'.',',');
          $total_a_pagar = (($cotizacion->total_pago_inicial + ($cotizacion->plazo - 1) * $cotizacion->pago_mensual_fijo) + ($cotizacion->pago_mensual_fijo / 1.16 * 0.16) + ($cotizacion->valor_residual_sin_iva * 1.16));
          $total_a_pagar = number_format($total_a_pagar,2,'.',',');
          $precio_sin_iva = number_format($cotizacion->valor_factura_sin_iva,2,'.',',');
          $valor_residual_con_iva = number_format(($cotizacion->valor_residual_sin_iva * 1.16),2,'.',',');
          if($cotizacion->seguro == "financiado")
          {
            // $seguro = number_format($cotizacion->valor_seguro,2,'.',',');  
            function PMT($rate,$nper,$pv,$fv,$type)
            {              
              return (-$fv - $pv * pow(1 + $rate, $nper)) /(1 + $rate * $type) /((pow(1 + $rate, $nper) - 1) / $rate);
            }            
            $valor_pmt = PMT(($cotizacion->porcentaje_tasa/100/12),12,-($cotizacion->valor_seguro * 1.1),0,0);
            $seguro = number_format($valor_pmt,2,'.',',');
            $subtotal = number_format($cotizacion->subtotal ,2,'.',',');            
            $total_pago_inicial = number_format($cotizacion->total_pago_inicial,2,'.',',');
          }
          else{
            if($cotizacion->valor_seguro > 0)
            {            
              $seguro = (($cotizacion->valor_seguro / 1.16) + 800);
              
              $iva = (($cotizacion->subtotal + $seguro + $cotizacion->renta_en_deposito) * .16);
              $total_pago_inicial = ($cotizacion->subtotal + $seguro) + $cotizacion->renta_en_deposito + $iva;
              $iva = number_format($iva,2,'.',',');                           
              $subtotal = number_format($cotizacion->subtotal + $seguro ,2,'.',',');                       
              $total_pago_inicial = number_format($total_pago_inicial,2,'.',',');
              $seguro = number_format($seguro,2,'.',',');
            }
            else{
              $seguro = 0;
              $iva = (($cotizacion->subtotal + $seguro + $cotizacion->renta_en_deposito) * .16);
              $total_pago_inicial = ($cotizacion->subtotal + $seguro) + $cotizacion->renta_en_deposito + $iva;
              $iva = number_format($iva,2,'.',',');                           
              $subtotal = number_format($cotizacion->subtotal + $seguro ,2,'.',',');                       
              $total_pago_inicial = number_format($total_pago_inicial,2,'.',',');
              $seguro = number_format($seguro,2,'.',',');
            }
          }
          
          
          
          ?>
          <tr>
            <td colspan="4">Precio de referencia (con IVA)</td>
            <td class="total">${!! $precio_con_iva !!}</td>
          </tr>
          <tr>
            <td colspan="4">Precio de referencia (sin IVA)</td>
            <td class="total">${!! $precio_sin_iva !!}</td>
          </tr>
          <tr>
            <td colspan="4">Plazo mensual</td>
            <td class="total">{!! $cotizacion->plazo !!} meses</td>
          </tr>
          <tr>
            <td colspan="4" class="subtotal">Pago mensual fijo</td>
            <td class="subtotal">${!! $pago_mensual_fijo !!}</td>
          </tr>
          <tr>
            <td colspan="4">Pago inicial</td>
            <td class="total">${!! $pago_inicial !!}</td>
          </tr>
          <tr>
            <td colspan="4">Gastos de apertura</td>
            <td class="total">${!! $gastos_apertura !!}</td>
          </tr>
          <tr>
            <td colspan="4">Localizador</td>
            @if($cotizacion->localizador == "Si")
            <td class="total"><em>Incluido</em></td>
            @else
            <td class="total"><em>No aplica</em></td>
            @endif
          </tr>
          <tr>
          @if($cotizacion->seguro == "financiado")
          <td colspan="4">Seguro mensual</td>          
          <td class="total" id="segurito">${!! $seguro !!}</td>
          @else
          <td colspan="4">Seguro</td>
            @if($seguro > 0)          
            <td class="total" id="segurito">${!! $seguro !!} <em>Contado</em></td>
            @else
            <td class="total" id="segurito">${!! $seguro !!} <em>No aplica</em></td>
            @endif
          @endif
          </tr>
          <tr>
            <td colspan="4" class="subtotal">SUBTOTAL</td>
            <td class="subtotal"> ${!! $subtotal !!} </td>
          </tr>
          <tr>
            <td colspan="4">1er Renta</td>
            <td class="total">${!! $renta_en_deposito !!}</td>
          </tr> 
          <tr>
            <td colspan="4">IVA</td>
            <td class="total">${!! $iva !!}</td>
          </tr> 
          <tr>
            <td colspan="4" class="subtotal">TOTAL PAGO INICIAL</td>
            <td class="subtotal">${!! $total_pago_inicial !!}</td>
          </tr>           
          <tr>
            <td colspan="4" class="subtotal">VALOR RESIDUAL(SIN IVA)</td>
            <td class="subtotal">${!! $valor_residual_sin_iva !!}</td>
          </tr>
          <!-- <tr>
            <td colspan="4" class="grand total">VALOR RESIDUAL(CON IVA)</td>
            <td class="grand total">${!! $valor_residual_con_iva !!}</td>
          </tr> -->
          <!-- <tr>
            <td colspan="4" class="grand total">TOTAL A PAGAR</td>
            <td class="grand total">$ {!! $total_a_pagar !!} </td>
          </tr> -->
        </tbody>
      </table>
      <div id="notices">
        <div>Términos y Condiciones:</div>
        <div class="notice">
          <ul>
            <li>Las Rentas se pagarán mensualmente en las fechas establecidas en el Anexo de Arrendamiento.</li>
            <li>Esta cotización podrá ser modificada por el Arrendador hasta la firma del Contrato y Anexo de Arrendamiento.</li>
            <li>Los costos del seguro y localizador podrán ser pagados por el Arrendatario por anticipado o en pagos mensuales, según se indique en la cotización.</li>
            <li>La obtención de los permisos, licencias, placas o registros que se requieran para poseer o usar el Bien Arrendado, así como el pago de tenencias, contribuciones, impuestos o costos inherentes al mantenimiento del Bien Arrendado, serán responsabilidad del Arrendatario.</li>
          </ul>
        </div>
      </div>
    </main>
    <footer>
      Proleasing
    </footer>
  </body>
</html>