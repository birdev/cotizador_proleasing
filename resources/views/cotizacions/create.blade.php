@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" />
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
@endsection

@section('scripts')
<script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>

<script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>

<script src="https://unpkg.com/@material-ui/core/umd/material-ui.development.js" crossorigin="anonymous"></script>

<!-- <script type="text/babel" src="{!! asset('components/prueba.jsx') !!}"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.2.0/knockout-min.js"></script>

<!-- <script src="{!! asset('js/calculos.js') !!}"></script> -->
<?php

?>
<script>
    var d = new Date();
    
    var anio_actual = d.getFullYear();
    
    for(var x = 0; x < 30; x++)
    {
        $('#anio').append('<option value="'+ (anio_actual - x) + '">'+ (anio_actual - x) +'</option>');
    }

    $("#poliza_seguro_anual2").on({
    "focus": function(event) {
        $(event.target).select();
    },
    "keyup": function(event) {
        $(event.target).val(function(index, value) {
        return value.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    }
    });


    function darFormato(input)
    {
        return input.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
    }

    function validarCampoFactura()
    {
        function validaFactura(){
            if($("#valor_factura_con_iva").val() === '') {  
                $("#eFacturaIva").html('Es necesario ingresar un dato');            
                return false;
            }
            else{
                $("#eFacturaIva").html('');        
                return true;                
            }
        }        

        if(validaFactura() == false)
        {
            $('html,body').animate({
                scrollTop: $("#tope").offset().top
            }, 500);
            $("#formLocalizador").prop('checked', false); 
        }
        else{
            localizadorCostoMensual();
            actualizarValores();            
        }
    }

    function caracteresCorreoValido(email){            
        var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

        if (caract.test(email) == false){
            return false;
        }
        else{
            return true;
        }
    }

    $("#btnEnviar").click(function(){
        
        function validoNombre(){
            if ($('#nombre_cliente').val() === '') {
            $("#eNombreCliente").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#nombre_cliente").val().length < 3) { 
                    $("#eNombreCliente").html('El campo debe tener como mínimo 3 caracteres');                
                    return false;
                }
                {
                    $("#eNombreCliente").html('');
                    return true;
                }
            }
        }

        function validoApePat(){
            if ($('#apellido_paterno_cliente').val() === '') {
            $("#eApePat").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#apellido_paterno_cliente").val().length < 4) { 
                    $("#eApePat").html('El campo debe tener como mínimo 4 caracteres');
                    return false;
                }
                else{
                    $("#eApePat").html('');
                    return true;
                }
            }
        }

        function validoApeMat(){
            if ($('#apellido_materno_cliente').val() === '') {
            $("#eApeMat").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#apellido_materno_cliente").val().length < 4) { 
                    $("#eApeMat").html('El campo debe tener como mínimo 4 caracteres');
                    return false;
                }
                else{
                    $("#eApeMat").html('');
                    return true;
                }
            }
        }

        function validoEmpresa(){
            if ($('#empresa_cliente').val() === '') {
            $("#eEmpresa").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#empresa_cliente").val().length < 3) { 
                    $("#eEmpresa").html('El campo debe tener como mínimo 3 caracteres');
                    return false;
                }
                else{
                    $("#eEmpresa").html('');
                    return true;
                }
            }
        }
        
        function validoTelefono(){
            if ($('#telefono_cliente').val() === '') {
            $("#eTelefono").html('El campo es necesario');
            return false;
            } 
            else {
                if($("#telefono_cliente").val().length < 10) { 
                    $("#eTelefono").html('El telefóno debe tener como minimo 10 caracteres');   
                    return false;             
                }
                else{
                $("#eTelefono").html('');
                return true;
                }
            }
        }

        function validoCorreo(){
            if ($('#email_cliente').val() === '') {
            $("#eCorreo").html('El campo es necesario');
            return false;
            } 
            else {
                if(caracteresCorreoValido($('#email_cliente').val()))
                {
                    $("#eCorreo").html('');
                    return true;
                }
                else
                {
                    $("#eCorreo").html('El correo electrónico no es valido');
                    return false;
                }
                
            }
        }

        function validaBien(){
            if ($('#selectBienes').val() === null) {
                $("#eBien").html('Es necesario seleccionar un bien');
                return false;
            } 
            else {
                $("#eBien").html('');                            
                return true;
            }
        }

        function validaMarca(){
            // CAMPO DE MARCA 
            if ($('#marca').val() === '') {
                $("#eMarca").html('El campo es necesario');
                return false;
            } 
            else{
                if($("#marca").val().length < 3){
                    $("#eMarca").html('El campo debe tener como mínimo 3 caracteres');
                    return false;
                }
                else{
                $("#eMarca").html('');                            
                return true;
                }
            }
        }

        function valdiaModelo(){
            // CAMPO DE MODELO
            if ($('#modelo').val() === '') {
                $("#eModelo").html('El campo es necesario');
                return false;
            } 
            else{
                if($("#modelo").val().length < 3){
                    $("#eModelo").html('El campo debe tener como mínimo 3 caracteres');
                    return false;
                }
                else{
                $("#eModelo").html('');                            
                return true;
                }
            }
        }

        function validaAnio(){
            // CAMPO DE AÑO
            if ($('#anio').val() === null) {
                $("#eAnio").html('El campo es necesario');
                return false;
            } 
            else {
                $("#eAnio").html('');
                return true;        
            }
        }

        function validaFacturaIva(){
            // CAMPO DE VALOR A FACTURAR CON IVA
            if ($('#valor_factura_con_iva').val() === '') {
                $("#eFacturaIva").html('El campo es necesario');
                return false;
            } 
            else {
                $("#eFacturaIva").html('');                            
                return true;
            }
        }

        function validaSeguro(){
            if ($('#poliza_seguro_anual2').val() === '') {
                $("#ePoliza").html('Coloque 0 si no aplica');
                return false;
            }
            else{
                $("#ePoliza").html('');        
                return true;            
            }
        }

        if(validoNombre() == false || validoApePat() == false || validoApeMat() == false || validoEmpresa() == false || validoTelefono() == false || validoCorreo() == false || validaSeguro() == false)
        {
            $('html,body').animate({ scrollTop: 0 }, 500);
        }
        else if(validaBien() == false || validaMarca() == false || valdiaModelo() == false || validaAnio() == false || validaFacturaIva() == false){
            $('html,body').animate({
                scrollTop: $("#tope").offset().top
            }, 500);
        }
        else{
            $("#formCotizacion").submit();
        }

    });
</script>
<script>

    // SLIDER DE PLAZO inicial
    
    var valoresPlazosTemporal = {!! $valoresPlazo !!};
    var valoresPlazos = [];
    for(var x = 0; x < valoresPlazosTemporal.length;x++)
    {
        valoresPlazos[x] = valoresPlazosTemporal[x].valor;
    }
    $(function() {
        $( "#sliderPlazo" ).slider({
            value: 0, 
            min: 0, 
            max: valoresPlazos.length-1, 
            slide: function( event, ui ) {
            $( "#valoresPlazo" ).text(valoresPlazos[ui.value] + " meses" ); 
            $( "#valorPlazoOculto" ).text(valoresPlazos[ui.value]);
            $("#pago_mensual_fijo").val(pagoMensualFijo()); 
            $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
            $("#renta_en_deposito").val(rentaDeposito());
            $("#total_pago_inicial").val(totalPagoInicial()); 
            $("#valor_residual_sin_iva").val(valorResidualSinIva());
            $("#total_por_pagar").val(totalPorPagar());
            $("#plazo").val(valoresPlazos[ui.value]);
            $("#valor_residual_calculo").val(valorResidual());
            actualizarValores();
            }
        });
        $( "#valoresPlazo" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")] + " meses" );
        $( "#valorPlazoOculto" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
        $( "#plazo" ).val(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
        $( "#valor_residual_calculo" ).val(valorResidual()[$( "#sliderPlazo" ).slider( "value")]);
        $("#valor_residual_calculo").val(valorResidual());
        
    });

    //  funcion para crear el slider de plazos
    function crearSliderPlazo() {
        $(function() {
            $( "#sliderPlazo" ).slider({
                value: 0, 
                min: 0, 
                max: valoresPlazos.length-1, 
                slide: function( event, ui ) {
                $( "#valoresPlazo" ).text(valoresPlazos[ui.value] + " meses" ); 
                $( "#valorPlazoOculto" ).text(valoresPlazos[ui.value]);
                $("#pago_mensual_fijo").val(pagoMensualFijo()); 
                $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
                $("#renta_en_deposito").val(rentaDeposito()); 
                $("#total_pago_inicial").val(totalPagoInicial()); 
                $("#valor_residual_sin_iva").val(valorResidualSinIva());
                $("#total_por_pagar").val(totalPorPagar());
                $("#plazo").val(valoresPlazos[ui.value]);
                $("#valor_residual_calculo").val(valorResidual());
                actualizarValores();

                }
            });
            $( "#valoresPlazo" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")] + " meses" );
            $( "#valorPlazoOculto" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
            $( "#plazo" ).val(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
            $("#valor_residual_calculo").val(valorResidual());
            
        });
    }

    //SLIDER DE PAGO INCIAL
    // función para saber en que opción del select estoy
    function tipoBien(dato)
    {
        if(dato == "Automóviles")
        {
            // var valoresPagoInitGeneral = [3, 5, 10, 15, 20, 25, 30];
            var valoresPagoInitGeneral = {!! $valoresPagoInicialGeneral !!};
            var valoresPagoInit = [];
            for(var x = 0; x < valoresPagoInitGeneral.length;x++)
            {
                valoresPagoInit[x] = valoresPagoInitGeneral[x].valor;
            }
            var valorPagoInicial = valoresPagoInit[0];
            crearSliderPagoInit(valoresPagoInit,valorPagoInicial);
            crearSliderPlazo();
            var valorFactura =  $("#valor_factura_sin_iva").val();    
            var vpi = (valorPagoInicial / 100);
            var result = (vpi * valorFactura);
            $("#pago_inicial").val(result);
            $("#valor_residual_calculo").val(valorResidual());
            
        }
        else
        {
            // var valoresPagoInit = [20, 25, 30];
            var valoresPagoInitParticulares = {!! $valoresPagoInicialNoParticulares !!};
            var valoresPagoInit = [];
            for(var x = 0; x < valoresPagoInitParticulares.length;x++)
            {
                valoresPagoInit[x] = valoresPagoInitParticulares[x].valor;
            }
            var valorPagoInicial = valoresPagoInit[0];
            crearSliderPagoInit(valoresPagoInit,valorPagoInicial);   
            crearSliderPlazo();  
            var valorFactura =  $("#valor_factura_sin_iva").val();    
            var vpi = (valorPagoInicial / 100);
            var result = (vpi * valorFactura);
            $("#pago_inicial").val(result);
            $("#valor_residual_calculo").val(valorResidual());
            
        }

        if(dato == "Automóviles" || dato == "Flotillas" || dato == "Camiones")
        {
            // $("#localizadorValidado").show();                    
            $('#formLocalizador').prop('checked',true);
        }
        else
        {
            // $("#localizadorValidado").hide();
            $('#formLocalizador').prop('checked',false);
        }
    }

    // funcion para iniciar el slider de pago inicial la primera vez.
    $(function() {
        // var valoresPagoInit = [20, 25, 30];
        var valoresPagoInitParticulares = {!! $valoresPagoInicialNoParticulares !!};
        var valoresPagoInit = [];
        for(var x = 0; x < valoresPagoInitParticulares.length;x++)
        {
            valoresPagoInit[x] = valoresPagoInitParticulares[x].valor;
        }
        var valorPagoInicial = valoresPagoInit[0];
        $( "#sliderPagoInicial" ).slider({
            value: 0, 
            min: 0, 
            max: valoresPagoInit.length-1, 
            slide: function( event, ui ) {
            $( "#valoresPagoInicial" ).text(valoresPagoInit[ui.value] + "%" ); 
            $("#valorPagoInicialOculto").text(valoresPagoInit[ui.value]);
            valorPagoInicial = valoresPagoInit[ui.value];
            $("#pago_inicial").val(calPagoInit());
            $("#pago_mensual_fijo").val(pagoMensualFijo());
            $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
            $("#subtotal").val(subtotal());
            $("#gastos_de_apertura").val(gastosDeApertura());
            $("#iva").val(iva());
            $("#renta_en_deposito").val(rentaDeposito()); 
            $("#total_pago_inicial").val(totalPagoInicial()); 
            $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
            $("#total_por_pagar").val(totalPorPagar());
            $("#porcentaje_pago_inicial").val(valoresPagoInit[ui.value]);            
            actualizarValores();

            }
        });
        $( "#valoresPagoInicial" ).text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")] + "%" );
        $("#valorPagoInicialOculto").text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
        $("#porcentaje_pago_inicial").val(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);                
    });

    // funcion para crear el slider de pago inicial
    function crearSliderPagoInit(valoresPagoInit,valorPagoInicial){
        $(function() {
            $( "#sliderPagoInicial" ).slider({
                value: 0, 
                min: 0, 
                max: valoresPagoInit.length-1, 
                slide: function( event, ui ) {
                $( "#valoresPagoInicial" ).text(valoresPagoInit[ui.value] + "%" );
                $("#valorPagoInicialOculto").text(valoresPagoInit[ui.value]);
                valorPagoInicial = valoresPagoInit[ui.value];
                $("#pago_inicial").val(calPagoInit());
                $("#pago_mensual_fijo").val(pagoMensualFijo());
                $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
                $("#subtotal").val(subtotal());
                $("#gastos_de_apertura").val(gastosDeApertura());
                $("#iva").val(iva());
                $("#renta_en_deposito").val(rentaDeposito()); 
                $("#total_pago_inicial").val(totalPagoInicial());                                 
                $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
                $("#total_por_pagar").val(totalPorPagar());
                $("#porcentaje_pago_inicial").val(valoresPagoInit[ui.value]);
                actualizarValores();
                }
            });
            $( "#valoresPagoInicial" ).text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")] + "%" );
            $( "#valorPagoInicialOculto").text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
            $("#porcentaje_pago_inicial").val(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);        
        });
    }

    // funcion para ejecutar el calculo al ingresar datos en el valor de factura sin iva
    $("#valor_factura_sin_iva").keyup(function(event){       
        $("#pago_inicial").val(calPagoInit());
        $("#subtotal").val(subtotal());
        $("#gastos_de_apertura").val(gastosDeApertura());
        $("#iva").val(iva());
        $("#pago_mensual_fijo").val(pagoMensualFijo());
        $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#total_por_pagar").val(totalPorPagar());
    }); 

    // funcion para pasarle el valor a factura sin iva
    $("#valor_factura_con_iva").keyup(function(event){       
        var valor_factura_sin_iva = ($("#valor_factura_con_iva").val() / 1.16);

        var aux1 = darFormato($("#valor_factura_con_iva").val());

        $("#valor_factura_con_iva").val(aux1);

        aux1 = aux1.replace(',','');
        aux1 = aux1.replace(',','');
        aux1 = aux1.replace(',','');
        aux1 = aux1.replace(',','');        

        aux2 = (aux1 / 1.16);

        // // $("#valor_factura_sin_iva").val(valor_factura_sin_iva.toFixed(2));
        $("#valor_factura_sin_iva").val(aux2.toFixed(2));

        aux3 = darFormato($("#valor_factura_sin_iva").val());

        $("#valor_factura_sin_iva2").val(aux3);

        

        $("#pago_inicial").val(calPagoInit());
        $("#pago_inicial2").val(darFormato($("#pago_inicial").val()));

        $("#subtotal").val(subtotal());
        $("#subtotal2").val(darFormato($("#subtotal").val()));


        $("#gastos_de_apertura").val(gastosDeApertura());
        $("#gastos_de_apertura2").val(darFormato($("#gastos_de_apertura").val()));

        $("#iva").val(iva());
        $("#iva2").val(darFormato($("#iva").val()));
        
        $("#pago_mensual_fijo").val(pagoMensualFijo());
        

        $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
        $("#pago_mensual_fijo_sin_iva2").val(darFormato($("#pago_mensual_fijo_sin_iva").val()));

        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#renta_en_deposito2").val(darFormato($("#renta_en_deposito").val()));

        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#total_pago_inicial2").val(darFormato($("#total_pago_inicial").val()));

        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#valor_residual_sin_iva2").val(darFormato($("#valor_residual_sin_iva").val()));

        $("#total_por_pagar").val(totalPorPagar());
        var comision = {!! $comision->valor !!} / 100;
        $("#porcentaje_comision").val(comision);

        var vr_adicional = {!! $valorResidualAdicional->valor !!}        
        
        $("#porcentaje_adicional_vr").val(vr_adicional);
        
        var tasa = ({!! $tasa->valor !!});
        $("#porcentaje_tasa").val(tasa);
        $("#iva").val(iva());
        $("#iva2").val(darFormato($("#iva").val()));
        
    }); 

    $("#poliza_seguro_anual").keyup(function(event){
        actualizarValores();
    });

    $("#poliza_seguro_anual2").keyup(function(event){
        
        var aux_poliza = $("#poliza_seguro_anual2").val().toString();

        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace(',','');
        aux_poliza = aux_poliza.replace(',','');
        
        aux_poliza = parseFloat(aux_poliza);        

        $("#poliza_seguro_anual").val(aux_poliza.toFixed(2));  
        actualizarValores();
        actualizarValores();
    });

    // función para calcular el pago inicial 
    function calPagoInit(){
        var valorFactura =  $("#valor_factura_sin_iva").val();    
        var valorPagoInicial = $("#valorPagoInicialOculto").text();
        var vpi = (valorPagoInicial / 100);
        var result = (vpi * valorFactura);
        return result.toFixed(2);
    }

    // funcion para sacar el subtototal
    function subtotal() {
        var valorFactura = $("#valor_factura_sin_iva").val();
        var valorPagoInicial = $("#valorPagoInicialOculto").text();   
        var pago_inicial = (valorPagoInicial / 100) * valorFactura;   
        var comision = {!! $comision->valor !!} / 100;
        var importe = valorFactura * comision;   
        var localizador = localizadorCostoMensual();
        
        if( $('#formLocalizador').prop('checked') ) {         
            var subtotal = importe + pago_inicial + 650;            
        }
        else
        {
            var subtotal = importe + pago_inicial;
        }       
        return subtotal.toFixed(2);
    }

    

    // funcion para sacar el pago mensual fijo
    function pagoMensualFijo() {
        var valorFactura = $("#valor_factura_sin_iva").val();
        var pago_inicial = ($("#valorPagoInicialOculto").text() /100);
        var importe_monto_financiar = (pago_inicial * valorFactura );
        var monto_financiar = valorFactura - importe_monto_financiar;
        var plazo = $("#valorPlazoOculto").text();
        
        var valor_residual = valorResidual();
        getValorResidual(valor_residual);
        var importe_valor_residual = ((valor_residual /100) * valorFactura);
        var tasa = ({!! $tasa->valor !!} / 100);
        var pago = PMT((tasa/12),plazo,(-(monto_financiar)),importe_valor_residual);
                
        var localizador = localizadorCostoMensual();
        var seguro = polizaSeguroAnual();
        
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            var iva = ((pago + localizador + seguro) * .16);
            var result = pago + iva + localizador + seguro;
        }
        else{
            var iva = ((pago + localizador) * .16);
            var result = pago + iva + localizador;
        }
        return result.toFixed(2);
    }


    function pagoMensualFijoSinIva() {
        var valorFactura = $("#valor_factura_sin_iva").val();
        var pago_inicial = ($("#valorPagoInicialOculto").text() /100);
        var importe_monto_financiar = (pago_inicial * valorFactura );
        var monto_financiar = valorFactura - importe_monto_financiar;
        var plazo = $("#valorPlazoOculto").text();

        var valor_residual = valorResidual();
        getValorResidual(valor_residual);
        var importe_valor_residual = ((valor_residual /100) * valorFactura);
        var tasa = ({!! $tasa->valor !!} / 100);        
        var pago = PMT((tasa/12),plazo,(-(monto_financiar)),importe_valor_residual);
        
        var localizador = localizadorCostoMensual();
        var seguro = polizaSeguroAnual();
                 
        var iva = (pago * .16);
        
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            var result = pago + localizador + seguro;
        }
        else{
            var result = pago + localizador;
        }

               

        return result.toFixed(2);
    }

    var valor_localizador = {!! $localizador->valor   !!}
    valor_localizador = parseInt(valor_localizador);
    function localizadorCostoMensual()
    {   
        var localizador = 0;

        if( $('#formLocalizador').prop('checked') ) {
            localizador = valor_localizador * 1.15;            
        }
        else
        {
            localizador = 0;
        }
        return localizador;
    }

  

    function actualizarValores()
    {
        var valor_factura_sin_iva = ($("#valor_factura_con_iva").val() / 1.16);
        
        var aux1 = darFormato($("#valor_factura_con_iva").val());

        $("#valor_factura_con_iva").val(aux1);

        aux1 = aux1.replace(',','');
        aux1 = aux1.replace(',','');
        aux1 = aux1.replace(',','');
        aux1 = aux1.replace(',','');        

        aux2 = (aux1 / 1.16);

        var aux3 = parseFloat(aux2);
        
        $("#valor_factura_sin_iva").val(aux3.toFixed(2));


        // $("#valor_factura_sin_iva").val(valor_factura_sin_iva.toFixed(2));
        $("#pago_inicial").val(calPagoInit());
        $("#pago_inicial2").val(darFormato($("#pago_inicial").val()));

        $("#subtotal").val(subtotal());
        $("#subtotal2").val(darFormato($("#subtotal").val()));

        $("#gastos_de_apertura").val(gastosDeApertura());
        $("#gastos_de_apertura2").val(darFormato($("#gastos_de_apertura").val()));

        $("#iva").val(iva());
        $("#iva2").val(darFormato($("#iva").val()));

        $("#pago_mensual_fijo").val(pagoMensualFijo());

        $("#pago_mensual_fijo_sin_iva").val(pagoMensualFijoSinIva());
        $("#pago_mensual_fijo_sin_iva2").val(darFormato($("#pago_mensual_fijo_sin_iva").val()));

        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#renta_en_deposito2").val(darFormato($("#renta_en_deposito").val()));

        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#total_pago_inicial2").val(darFormato($("#total_pago_inicial").val()));

        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#valor_residual_sin_iva2").val(darFormato($("#valor_residual_sin_iva").val()));

        $("#total_por_pagar").val(totalPorPagar());

        var comision = {!! $comision->valor !!} / 100;
        $("#porcentaje_comision").val(comision);

        var vr_adicional = {!! $valorResidualAdicional->valor !!}
        $("#porcentaje_adicional_vr").val(vr_adicional);
        
        var tasa = ({!! $tasa->valor !!});
        $("#porcentaje_tasa").val(tasa);

        $("#iva").val(iva());
        $("#iva2").val(darFormato($("#iva").val()));
    }

    // funcion para sacar el valor residual
    function getValorResidual(valor_residual) {
        $("#valor_residual_calculo").val(valor_residual);
    }

    // funcion replica de PMT de excel
    function PMT(rate, nperiod, pv, fv, type) 
    { 
        if (!fv) fv = 0; if (!type) type = 0; if (rate == 0) return -(pv + fv)/nperiod; var pvif = Math.pow(1 + rate, nperiod); var pmt = rate / (pvif - 1) * -(pv * pvif + fv); if (type == 1) { pmt /= (1 + rate); }; return pmt; 
    }

    function iva(){
        var subtotal = $("#subtotal").val();
        var iva = (subtotal * .16);

        var primer_renta = rentaDeposito(); 
        
        var iva_primer_renta = (primer_renta * .16);

        // var total = (iva_primer_renta +iva);
        var total = (iva_primer_renta + iva);
        
        return iva.toFixed(2);
    }

    function rentaDeposito() {
        var pagoMensualFijo = $("#pago_mensual_fijo").val();
        var rentaDeposito = (pagoMensualFijo / 1.16);
        
        return rentaDeposito.toFixed(2);
    }

    function totalPagoInicial(){
        var subtotal = $("#subtotal").val();
        var iva = $("#iva").val();
        var rentaDeposito = $("#renta_en_deposito").val();
        var total = parseFloat(subtotal) + parseFloat(iva) + parseFloat(rentaDeposito);
                   
        return total.toFixed(2);
    }

    var vr_adicional = {!! $valorResidualAdicional->valor !!}
    vr_adicional = parseInt(vr_adicional);
    
    function valorResidualSinIva() {
        var valor_residual = valorResidual();
        var valor_sin_iva = valor_residual + vr_adicional;
        var valorFactura = $("#valor_factura_sin_iva").val();
        var final = (valor_sin_iva/100) * valorFactura
        return final.toFixed(2);
    }


    var array_autos = {!! $valorResidualAutos !!};
    var array_flotillas = {!! $valorResidualFlotillas !!};
    var array_camiones = {!! $valorResidualCamiones !!};
    var array_maquinaria = {!! $valorResidualMaquinaria !!};
    var array_electronica = {!! $valorResidualElectronica !!};
    
    function valorResidual() {
        var valor_residual = 0;
        var tipo_bien = $("#selectBienes").val();
        var plazo = $("#valorPlazoOculto").text();

        if(tipo_bien == "Automóviles")
        {
            array_autos.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });
            
            // if(plazo == 12)
            // {
            //     valor_residual = 50;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 40;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 30;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 20;
            // }
        }
        else if(tipo_bien == "Flotillas")
        {
            array_flotillas.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 40;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 30;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 10;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 5;
            // }
        }
        else if(tipo_bien == "Camiones")
        {            
            
            array_camiones.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 35;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 25;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 15;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 5;
            // }
        }
        else if(tipo_bien == "Maquinaria, Mobiliario y Equipo")
        {

            array_maquinaria.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 40;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 30;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 10;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 0;
            // }
        }
        else
        {

            array_electronica.forEach(element => {
                if(plazo == element.plazo_vr)
                {
                    valor_residual = element.valor;
                    valor_residual = parseInt(valor_residual);
                }
            });

            // if(plazo == 12)
            // {
            //     valor_residual = 20;
            // }
            // else if(plazo == 24)
            // {
            //     valor_residual = 10;
            // }
            // else if(plazo == 36)
            // {
            //     valor_residual = 0;
            // }
            // else if(plazo == 48)
            // {
            //     valor_residual = 0;
            // }
        }
        return valor_residual;
    } 

    function totalPorPagar(){
        var total_pago_inicial = $("#total_pago_inicial").val();
        var plazo = $( "#valorPlazoOculto" ).text();
        var pago_mensual_fijo = $("#pago_mensual_fijo").val(); 
        var valor_residual_sin_iva = $("#valor_residual_sin_iva").val();
        var total_a_pagar = ((parseFloat(total_pago_inicial) + (plazo - 1) * parseFloat(pago_mensual_fijo) + (parseFloat(pago_mensual_fijo) / 1.16 * .16) + parseFloat(valor_residual_sin_iva) * 1.16)); 
        
        return total_a_pagar.toFixed(2);
    }

    function gastosDeApertura()
    {
        var valorFactura = $("#valor_factura_sin_iva").val();   
        var comision = {!! $comision->valor !!} / 100;
        var importe = valorFactura * comision;
        
        if( $('#formLocalizador').prop('checked') ) {
            var localizador = 650;
        }
        else{
            localizador = 0;
        }
        var gastosApertura = localizador + importe;

        return gastosApertura.toFixed(2);
    }

    function polizaSeguroAnual()
    {
        var monto_poliza = $('#poliza_seguro_anual').val();
    
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            monto_poliza = monto_poliza * 1.1;
        }
        else{
            monto_poliza = monto_poliza;
        }        
        var seguro = 0;
        var tasa = ({!! $tasa->valor !!} / 100);
        
        if($('input:radio[name=formRadios]:checked').val() == "financiado")
        {
            seguro = PMT(tasa/12,12,-monto_poliza,0,0);                                    
        }
        else
        {
            if(monto_poliza == 0)
            {
                seguro = 0;
            }
            else{
                seguro = (monto_poliza / 1.16) + 800;
            }            
            
        }  
        return parseFloat(seguro.toFixed(2));
    }
</script>

@endsection

@section('content')
    <section class="content-header">
        <div class="page-header row no-gutters py-4">
            <span class="text-uppercase page-subtitle">
                
            </span>
        </div>
    </section>
    <div class="content" style="width: 100%;">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    <div class="main-content-container container-fluid px-4">
                    {!! Form::open(['route' => 'cotizacions.store','id' => 'formCotizacion']) !!}

                        @include('cotizacions.fields')

                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
