<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Nueva cotización</span>
        <h3 class="page-title">Datos del asesor</h3>
        </div>
    </div>
</div>
<!-- Id Field -->
<!-- <div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cotizacion->id !!}</p>
</div> -->
<div class="form-group col-sm-12 row">
    <!-- User Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('user_id', 'Asesor que atendio:') !!}
        <p>{!! $cotizacion->user->name !!} {!! $cotizacion->user->apellido_paterno !!} {!! $cotizacion->user->apellido_paterno !!}</p>
    </div> 
</div>
<!-- Uri Pdf Field -->
<!-- <div class="form-group">
    {!! Form::label('uri_pdf', 'Uri Pdf:') !!}
    <p>{!! $cotizacion->uri_pdf !!}</p>
</div> -->

<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Cliente</span>
        <h3 class="page-title">Información</h3>
        </div>
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Nombre Cliente Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('nombre_cliente', 'Nombre:') !!}
        <p>{!! $cotizacion->nombre_cliente !!} {!! $cotizacion->apellido_paterno_cliente !!} {!! $cotizacion->apellido_materno_cliente !!}</p>
    </div>

    <!-- Apellido Paterno Cliente Field -->
    <!-- <div class="form-group">
        {!! Form::label('apellido_paterno_cliente', 'Apellido Paterno Cliente:') !!}
        <p>{!! $cotizacion->apellido_paterno_cliente !!}</p>
    </div> -->

    <!-- Apellido Materno Cliente Field -->
    <!-- <div class="form-group">
        {!! Form::label('apellido_materno_cliente', 'Apellido Materno Cliente:') !!}
        <p>{!! $cotizacion->apellido_materno_cliente !!}</p>
    </div> -->

    <!-- Empresa Cliente Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('empresa_cliente', 'Empresa:') !!}
        <p>{!! $cotizacion->empresa_cliente !!}</p>
    </div>

    <!-- Telefono Cliente Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('telefono_cliente', 'Teléfono:') !!}
        <p>{!! $cotizacion->telefono_cliente !!}</p>
    </div>

    <!-- Email Cliente Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('email_cliente', 'Correo electrónico:') !!}
        <p>{!! $cotizacion->email_cliente !!}</p>
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Condiciones de arrendamiento</span>
        <h3 class="page-title">Datos del bien</h3>
        </div>
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Bien Id Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('bien_id', 'Categoría:') !!}
        <p>{!! $cotizacion->bien->nombre !!}</p>
    </div>

    <!-- Bien Id Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('bien_id', 'Marca:') !!}
        <p>{!! $cotizacion->marca !!}</p>
    </div>

    <!-- Bien Id Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('bien_id', 'Modelo:') !!}
        <p>{!! $cotizacion->modelo !!}</p>
    </div>

    <!-- Bien Id Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('bien_id', 'Año:') !!}
        <p>{!! $cotizacion->anio !!}</p>
    </div>
</div>
<div class="form-group col-sm-12">
    <!-- Valor Factura Sin Iva Field -->
    <div class="form-group col-sm-12" style="text-align:center">
        {!! Form::label('valor_factura_sin_iva', 'Valor Factura Sin Iva:') !!}
        <p>{!! $cotizacion->valor_factura_sin_iva !!}</p>
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Condiciones de arrendamiento</span>
        <h3 class="page-title">Tasas</h3>
        </div>
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Porcentaje Comision Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('porcentaje_comision', 'Porcentaje Comision:') !!}
        <p>{!! $cotizacion->porcentaje_comision !!}</p>
    </div>

    <!-- Plazo Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('plazo', 'Plazo:') !!}
        <p>{!! $cotizacion->plazo !!}</p>
    </div>

    <!-- Porcentaje Pago Inicial Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('porcentaje_pago_inicial', 'Porcentaje Pago Inicial:') !!}
        <p>{!! $cotizacion->porcentaje_pago_inicial !!}</p>
    </div>

    <!-- Valor Residual Calculo Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('valor_residual_calculo', 'Valor Residual Calculo:') !!}
        <p>{!! $cotizacion->valor_residual_calculo !!}</p>
    </div>

    <!-- Porcentaje Adicional Vr Impreso Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('porcentaje_adicional_vr_impreso', 'Porcentaje Adicional Vr Impreso:') !!}
        <p>{!! $cotizacion->porcentaje_adicional_vr_impreso !!}</p>
    </div>

    <!-- Porcentaje Tasa Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('porcentaje_tasa', 'Porcentaje Tasa:') !!}
        <p>{!! $cotizacion->porcentaje_tasa !!}</p>
    </div>

    <!-- Seguro Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('seguro', 'Seguro:') !!}
        <p>{!! $cotizacion->seguro !!}</p>
    </div>

    <!-- Localizador Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('Localizador', 'Localizador:') !!}
        <p>{!! $cotizacion->localizador !!}</p>
    </div>
</div>

<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Condiciones de arrendamiento</span>
        <h3 class="page-title">Valores finales</h3>
        </div>
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Pago Inicial Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('pago_inicial', 'Pago Inicial:') !!}
        <p>{!! $cotizacion->pago_inicial !!}</p>
    </div>

    <!-- Subtotal Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('subtotal', 'Subtotal:') !!}
        <p>{!! $cotizacion->subtotal !!}</p>
    </div>

    <!-- Pago Mensual Fijo Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('pago_mensual_fijo', 'Pago Mensual Fijo:') !!}
        <p>{!! $cotizacion->pago_mensual_fijo !!}</p>
    </div>

    <!-- Iva Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('iva', 'Iva:') !!}
        <p>{!! $cotizacion->iva !!}</p>
    </div>

    <!-- Renta En Deposito Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('renta_en_deposito', '1er Renta:') !!}
        <p>{!! $cotizacion->renta_en_deposito !!}</p>
    </div>

    <!-- Total Pago Inicial Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('total_pago_inicial', 'Total Pago Inicial:') !!}
        <p>{!! $cotizacion->total_pago_inicial !!}</p>
    </div>

    <!-- Valor Residual Sin Iva Field -->
    <div class="form-group col-sm-12" style="text-align:center">
        {!! Form::label('valor_residual_sin_iva', 'Valor Residual Sin Iva:') !!}
        <p>{!! $cotizacion->valor_residual_sin_iva !!}</p>
    </div>
</div>
<!-- Created At Field -->
<!-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cotizacion->created_at !!}</p>
</div> -->

<!-- Updated At Field -->
<!-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cotizacion->updated_at !!}</p>
</div> -->

<!-- Deleted At Field -->
<!-- <div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $cotizacion->deleted_at !!}</p>
</div> -->