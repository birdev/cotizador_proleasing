@if(count($cotizacions) != 0)
<table class="table table-responsive hover" id="cotizacions-table">
    <thead>
        <tr>
        <th>Fecha</th>
        <th>Hora</th>
        <th>Nombre Cliente</th>
        <th>Apellido Paterno Cliente</th>
        <th>Apellido Materno Cliente</th>
        <th>Empresa Cliente</th>
        <th>Bien</th>
        <th>Valor Factura Sin Iva</th>
        <th>Pago Mensual Fijo</th>        
        <th>Valor Residual Sin Iva</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody >
    @foreach($cotizacions as $cotizacion)
    <?php
    $cotizacion->valor_factura_sin_iva = number_format(($cotizacion->valor_factura_sin_iva),2,'.',',');
    $cotizacion->pago_mensual_fijo = number_format(($cotizacion->pago_mensual_fijo),2,'.',',');
    $cotizacion->valor_residual_sin_iva = number_format(($cotizacion->valor_residual_sin_iva),2,'.',',');
    $fecha = $cotizacion->created_at->format('Y/n/j');
    $hora = $cotizacion->created_at->format('h:i:s/A');
    ?>
        <tr>
            <td>{!! $fecha !!}</td>
            <td>{!! $hora !!}</td>
            <td>{!! $cotizacion->nombre_cliente !!}</td>
            <td>{!! $cotizacion->apellido_paterno_cliente !!}</td>
            <td>{!! $cotizacion->apellido_materno_cliente !!}</td>
            <td>{!! $cotizacion->empresa_cliente !!}</td>
            <td>{!! $cotizacion->bien->nombre !!}</td>
            <td>${!! $cotizacion->valor_factura_sin_iva !!}</td>
            <td>${!! $cotizacion->pago_mensual_fijo !!}</td>
            <td>${!! $cotizacion->valor_residual_sin_iva !!}</td>
            
            <td>
                {!! Form::open(['route' => ['cotizacions.destroy', $cotizacion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a target="_blank" href="{!! asset('pdf') !!}/{!! $cotizacion->id !!}" class="btn btn-dark btn-xs"><i class="far fa-file-pdf"></i></a>      
                    <a href="{!! route('cotizacions.show', [$cotizacion->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-eye"></i></a>
                    <!-- <a href="{!! route('cotizacions.edit', [$cotizacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a> -->
                    {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Seguro de eliminar esta cotización?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endif