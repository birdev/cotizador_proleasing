@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cotizacion
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cotizacions.show_fields')              
                    <!-- <a href="{!! asset('jiji') !!}/{!! $cotizacion->id !!}" class="btn btn-danger">jijiji</a>                     -->
                    <a href="{!! route('cotizacions.index') !!}" class="btn btn-info">Regresar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
