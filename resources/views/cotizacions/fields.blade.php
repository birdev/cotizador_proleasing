<?php
$bienes = \App\Models\Bien::all();
?>

<style>
.errorField{
        color: #000;
        font-family: 'Roboto', sans-serif;
        padding-left: 10px;
        font-weight: 900 !important;
    }
</style>
<!-- --------------------DATOS DEL CLIENTE---------------------------------------------------------------- -->
<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Nueva cotización</span>
        <h3 class="page-title">Datos del cliente</h3>
        </div>
    </div>
</div>

<div class="form-group col-sm-12 row">
<!-- Nombre Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('nombre_cliente', 'Nombre:') !!}
        {!! Form::text('nombre_cliente', null, ['class' => 'form-control']) !!}
        <span id="eNombreCliente" class="errorField"></span>
    </div>

    <!-- Apellido Paterno Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('apellido_paterno_cliente', 'Apellido paterno:') !!}
        {!! Form::text('apellido_paterno_cliente', null, ['class' => 'form-control']) !!}
        <span id="eApePat" class="errorField"></span>
    </div>

    <!-- Apellido Materno Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('apellido_materno_cliente', 'Apellido materno:') !!}
        {!! Form::text('apellido_materno_cliente', null, ['class' => 'form-control']) !!}
        <span id="eApeMat" class="errorField"></span>
    </div>
</div>
<div class="form-group col-sm-12 row">
    <!-- Empresa Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('empresa_cliente', 'Empresa:') !!}
        {!! Form::text('empresa_cliente', null, ['class' => 'form-control']) !!}
        <span id="eEmpresa" class="errorField"></span>
    </div>
    <!-- Telefono Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('telefono_cliente', 'Telefono:') !!}
        {!! Form::text('telefono_cliente', null, ['class' => 'form-control']) !!}
        <span id="eTelefono" class="errorField"></span>
    </div>

    <!-- Email Cliente Field -->
    <div class="form-group col-sm-4">
        {!! Form::label('email_cliente', 'Correo electrónico:') !!}
        {!! Form::text('email_cliente', null, ['class' => 'form-control']) !!}
        <span id="eCorreo" class="errorField"></span>
    </div>
    <div id="tope"></div>
</div>
<!-- --------------------CONDICIONES DE ARRENDAMIENTO---------------------------------------------------------------- -->
<!-- --------------------DATOS DEL BIEN A ARRENDAR---------------------------------------------------------------- -->

<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Condiciones de arrendamiento</span>
        <h3 class="page-title">Datos del bien</h3>
        </div>
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Bien Id Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('bien_id', 'Bien:') !!}
        <!-- {!! Form::number('bien_id', null, ['class' => 'form-control']) !!} -->
        <select class="form-control" id="selectBienes" name="bien" onchange="tipoBien(this.value)">
            <option value="" disabled selected>Selecciona un bien a arrendar</option>
                @foreach($bienes as $bien)                
                        <option value='{!! $bien->nombre !!}'>{!! $bien->nombre !!}</option>
                @endforeach
        </select>   
        <span class="errorField" id="eBien"></span>
    </div>
    <!-- onchange="redireccionar(this.value)" -->
    <!-- Marca Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('marca', 'Marca:') !!}
        {!! Form::text('marca', null, ['class' => 'form-control']) !!}
        <span class="errorField" id="eMarca"></span>
    </div>

    <!-- Modelo Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('modelo', 'Modelo:') !!}
        {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
        <span class="errorField" id="eModelo"></span>
    </div>

    <!-- Año Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('anio', 'Año:') !!}
        <!-- {!! Form::number('anio', null, ['class' => 'form-control']) !!} -->
        <select class="form-control" id="anio" name="anio">
            <option value="" disabled selected>Selecciona un año</option>
        </select>   
        <span class="errorField" id="eAnio"></span>
    </div>
</div>
<div class="form-group col-sm-12 row">
    <!-- Valor Factura Sin Iva Field -->
    <div class="form-group col-sm-3">
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('valor_factura_con_iva', 'Valor Factura Con Iva:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::text('valor_factura_con_iva', null, ['class' => 'form-control']) !!}
        </div>
        <span id="eFacturaIva" class="errorField"></span>
    </div>
    <div class="form-group col-sm-3">
    </div>
</div>
<div class="form-group col-sm-12 row">
    <div class="form-group col-sm-3"></div>
    <div class="form-group col-sm-6">
        {!! Form::label('valor_factura_sin_iva', 'Valor Factura Sin Iva:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('valor_factura_sin_iva', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('valor_factura_sin_iva2', null, ['id' => 'valor_factura_sin_iva2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>
    <div class="form-group col-sm-3"></div>
</div>

<!-- --------------------DATOS DEL BIEN A ARRENDAR---------------------------------------------------------------- -->
<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Condiciones de arrendamiento</span>
        <h3 class="page-title">Ajuste de valores</h3>
        </div>
    </div>
</div>

<!-- Porcentaje Comision Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('porcentaje_comision', 'Porcentaje Comision:') !!}
    <div id="sliderComision"></div>
    <center><h1 id="valorcomision">0%</h1></center>   
</div> -->

<div class="form-group col-sm-12 row">
    <!-- Plazo Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('plazo', 'Plazo:') !!}
        <!-- {!! Form::number('plazo', null, ['class' => 'form-control']) !!} -->
        <div id="sliderPlazo"></div> 
        <center><h1 id="valoresPlazo">0</h1></center>
        <h1 id="valorPlazoOculto" style="display:none"></h1>
        {!! Form::number('plazo', null, ['class' => 'form-control', 'style' => 'display:none']) !!}    
    </div>

    <!-- Porcentaje Pago Inicial Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('porcentaje_pago_inicial', 'Porcentaje Pago Inicial:') !!}
        <div id="sliderPagoInicial"></div> 
        <center><h1 id="valoresPagoInicial">0%</h1></center>
        <h1 id="valorPagoInicialOculto" style="display:none"></h1>
        {!! Form::number('porcentaje_pago_inicial', null, ['id' => 'porcentaje_pago_inicial','class' => 'form-control','style' => 'display:none']) !!}    
    </div>
</div>

<!-- Valor Residual Calculo Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('valor_residual_calculo', 'Valor Residual Calculo:') !!}
    {!! Form::number('valor_residual_calculo', null, ['class' => 'form-control']) !!}
</div> -->
{!! Form::number('valor_residual_calculo', null, ['class' => 'form-control','style' => 'display:none']) !!}

<!-- Porcentaje Adicional Vr Impreso Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('porcentaje_adicional_vr_impreso', 'Porcentaje Adicional Vr Impreso:') !!}
    {!! Form::number('porcentaje_adicional_vr_impreso', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Porcentaje Tasa Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('porcentaje_tasa', 'Porcentaje Tasa:') !!}
    {!! Form::number('porcentaje_tasa', null, ['class' => 'form-control']) !!}
</div> -->

<div class="form-group col-sm-12 row">
    <div class="form-group col-sm-6">
        {!! Form::label('poliza_seguro_anual', 'Poliza de seguro anual: ') !!}                
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('poliza_seguro_anual', 0, ['class' => 'form-control']) !!}
            {!! Form::text('poliza_seguro_anual2', '0', ['id' => 'poliza_seguro_anual2','class' => 'form-control custom-form','style' => 'width:150px']) !!}
            <br>
            <span id="ePoliza" class="errorField"></span>
        </div>    
        <div>
            <div class="custom-control custom-radio mb-1">
                <input type="radio" id="formContado" name="formRadios" class="custom-control-input" checked value="contado" onclick="actualizarValores();polizaSeguroAnual();actualizarValores();">
                <label class="custom-control-label" for="formContado">Contado</label>
            </div>
            <div class="custom-control custom-radio mb-1">
                <input type="radio" id="formFinanciado" name="formRadios" class="custom-control-input" value="financiado" onclick="actualizarValores();polizaSeguroAnual();actualizarValores();">
                <label class="custom-control-label" for="formFinanciado">Financiado</label>
            </div>
            <div>
            <a  class="btn btn-primary" href="https://autos.interesse.com.mx/telerentaventas" target="_blank">Cotizar en linea</button></a>
            </div>
        </div>
    </div>        
</div>

<div id="localizadorValidado" style="display:none">
    <div class="form-group col-sm-12 row">
        <div class="form-group col-sm-6">
            {!! Form::label('localizador', 'Localizador(costo mensual): ') !!}
            <br>
            <div class="custom-control custom-checkbox mb-1">
                <input type="checkbox" class="custom-control-input" id="formLocalizador" name="formLocalizador" value="localizador" onclick="validarCampoFactura();">
                <label class="custom-control-label" for="formLocalizador">Agregar</label>
            </div>        
        </div>
    </div>
</div>
<!-- --------------------VALORS RESULTANTES ---------------------------------------------------------------- -->

<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Cotización</span>
        <h3 class="page-title">Valores resultantes</h3>
        </div>
    </div>
</div>


<div class="form-group col-sm-12 row">
    <!-- Pago Inicial Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('pago_inicial', 'Pago Inicial:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('pago_inicial', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('pago_inicial2', null, ['id' => 'pago_inicial2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>

    <!-- Gastos de apertura Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('gastos_de_apertura', 'Gastos de apertura:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('gastos_de_apertura', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('gastos_de_apertura2', null, ['id' => 'gastos_de_apertura2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>

    <!-- Pago Mensual Fijo Sin iva Field -->
    <div class="form-group col-sm-2">
    {!! Form::label('pago_mensual_fijo_sin_iva', 'Pago mensual(sin iva):') !!}
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('pago_mensual_fijo_sin_iva', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('pago_mensual_fijo_sin_iva2', null, ['id' => 'pago_mensual_fijo_sin_iva2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>

    <!-- Renta En Deposito Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('renta_en_deposito', 'Primer renta:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('renta_en_deposito', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('renta_en_deposito2', null, ['id' => 'renta_en_deposito2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>

    <!-- Subtotal Field -->
    <div class="form-group col-sm-2">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('subtotal', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('subtotal2', null, ['id' => 'subtotal2', 'class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>

    <!-- Iva Field -->
    <div class="form-group col-sm-2">
        {!! Form::label('iva', 'Iva:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('iva', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('iva2', null, ['id' => 'iva2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>


</div>

<div class="form-group col-sm-12 row">
    <!-- Valor Residual Sin Iva Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('valor_residual_sin_iva', 'Valor Residual Sin Iva:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('valor_residual_sin_iva', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('valor_residual_sin_iva2', null, ['id' => 'valor_residual_sin_iva2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>
    
    <!-- <div class="form-group col-sm-6">
        {!! Form::label('total_por_pagar', 'Total por pagar:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::number('total_por_pagar', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div> -->

        <!-- Total Pago Inicial Field -->
    <div class="form-group col-sm-6">
    {!! Form::label('total_pago_inicial', 'Total Pago Inicial:') !!}
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div>
            {!! Form::hidden('total_pago_inicial', null, ['class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::text('total_pago_inicial2', null, ['id' => 'total_pago_inicial2','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
        </div>
    </div>
</div>


    <!-- Pago Mensual Fijo Field -->
    <!-- <div class="form-group col-sm-2"> -->
        <!-- {!! Form::label('pago_mensual_fijo', 'Pago Mensual Fijo:') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            <span class="input-group-text">$</span>
            </div> -->
            {!! Form::number('pago_mensual_fijo', null, ['class' => 'form-control','style' => 'visibility:hidden','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::hidden('porcentaje_comision', null, ['id' => 'porcentaje_comision','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::hidden('porcentaje_adicional_vr', null, ['id' => 'porcentaje_adicional_vr','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            {!! Form::hidden('porcentaje_tasa', null, ['id' => 'porcentaje_tasa','class' => 'form-control','readonly' => 'readonly','step'=> 'any']) !!}
            
        <!-- </div>
    </div> -->

<!-- Submit Field -->

<div class="form-group col-sm-12" style="text-align:center">
    <button type="button" id="btnEnviar" class="btn btn-primary">Guardar</button>
    <a href="{!! route('cotizacions.index') !!}" class="btn btn-danger">Cancelar</a>
</div>