// SLIDER DE PLAZO inicial
var valoresPlazos = [12, 24, 36, 48];
 
 $(function() {
     $( "#sliderPlazo" ).slider({
         value: 0, 
         min: 0, 
         max: valoresPlazos.length-1, 
         slide: function( event, ui ) {
         $( "#valoresPlazo" ).text(valoresPlazos[ui.value] + " meses" ); 
         $( "#valorPlazoOculto" ).text(valoresPlazos[ui.value]);
         $("#pago_mensual_fijo").val(pagoMensualFijo()); 
         $("#renta_en_deposito").val(rentaDeposito()); 
         $("#total_pago_inicial").val(totalPagoInicial()); 
         $("#valor_residual_sin_iva").val(valorResidualSinIva());
         $("#total_por_pagar").val(totalPorPagar());
         $("#plazo").val(valoresPlazos[ui.value]);
         $("#valor_residual_calculo").val(valorResidual());
         
         }
     });
     $( "#valoresPlazo" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")] + " meses" );
     $( "#valorPlazoOculto" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
     $( "#plazo" ).val(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
     $( "#valor_residual_calculo" ).val(valorResidual()[$( "#sliderPlazo" ).slider( "value")]);
     $("#valor_residual_calculo").val(valorResidual());
     
 });

//  funcion para crear el slider de plazos
 function crearSliderPlazo() {
    $(function() {
        $( "#sliderPlazo" ).slider({
            value: 0, 
            min: 0, 
            max: valoresPlazos.length-1, 
            slide: function( event, ui ) {
            $( "#valoresPlazo" ).text(valoresPlazos[ui.value] + " meses" ); 
            $( "#valorPlazoOculto" ).text(valoresPlazos[ui.value]);
            $("#pago_mensual_fijo").val(pagoMensualFijo()); 
            $("#renta_en_deposito").val(rentaDeposito()); 
            $("#total_pago_inicial").val(totalPagoInicial()); 
            $("#valor_residual_sin_iva").val(valorResidualSinIva());
            $("#total_por_pagar").val(totalPorPagar());
            $("#plazo").val(valoresPlazos[ui.value]);
            $("#valor_residual_calculo").val(valorResidual());
            

            }
        });
        $( "#valoresPlazo" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")] + " meses" );
        $( "#valorPlazoOculto" ).text(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
        $( "#plazo" ).val(valoresPlazos[$( "#sliderPlazo" ).slider( "value")]);
        $("#valor_residual_calculo").val(valorResidual());
        
    });
 }

//SLIDER DE PAGO INCIAL
// función para saber en que opción del select estoy
function tipoBien(dato)
{
    if(dato == "Particulares")
    {
        var valoresPagoInit = [3, 5, 10, 15, 20, 25, 30];
        var valorPagoInicial = 3;
        crearSliderPagoInit(valoresPagoInit,valorPagoInicial);
        crearSliderPlazo();
        var valorFactura =  $("#valor_factura_sin_iva").val();    
        var vpi = (valorPagoInicial / 100);
        var result = (vpi * valorFactura);
        $("#pago_inicial").val(result);
        $("#valor_residual_calculo").val(valorResidual());
        
    }
    else
    {
        var valoresPagoInit = [20, 25, 30];
        var valorPagoInicial = 20;
        crearSliderPagoInit(valoresPagoInit,valorPagoInicial);   
        crearSliderPlazo();  
        var valorFactura =  $("#valor_factura_sin_iva").val();    
        var vpi = (valorPagoInicial / 100);
        var result = (vpi * valorFactura);
        $("#pago_inicial").val(result);
        $("#valor_residual_calculo").val(valorResidual());
        
    }
}

// funcion para iniciar el slider de pago inicial la primera vez.
$(function() {
    var valoresPagoInit = [20, 25, 30];
    var valorPagoInicial = 20;
    $( "#sliderPagoInicial" ).slider({
        value: 0, 
        min: 0, 
        max: valoresPagoInit.length-1, 
        slide: function( event, ui ) {
        $( "#valoresPagoInicial" ).text(valoresPagoInit[ui.value] + "%" ); 
        $("#valorPagoInicialOculto").text(valoresPagoInit[ui.value]);
        valorPagoInicial = valoresPagoInit[ui.value];
        $("#pago_inicial").val(calPagoInit());
        $("#pago_mensual_fijo").val(pagoMensualFijo());
        $("#subtotal").val(subtotal());
        $("#iva").val(iva());
        $("#renta_en_deposito").val(rentaDeposito()); 
        $("#total_pago_inicial").val(totalPagoInicial()); 
        $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
        $("#total_por_pagar").val(totalPorPagar());
        $("#porcentaje_pago_inicial").val(valoresPagoInit[ui.value]);

        }
    });
    $( "#valoresPagoInicial" ).text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")] + "%" );
    $("#valorPagoInicialOculto").text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
    $("#porcentaje_pago_inicial").val(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
});

// funcion para crear el slider de pago inicial
function crearSliderPagoInit(valoresPagoInit,valorPagoInicial){
    $(function() {
        $( "#sliderPagoInicial" ).slider({
            value: 0, 
            min: 0, 
            max: valoresPagoInit.length-1, 
            slide: function( event, ui ) {
            $( "#valoresPagoInicial" ).text(valoresPagoInit[ui.value] + "%" );
            $("#valorPagoInicialOculto").text(valoresPagoInit[ui.value]);
            valorPagoInicial = valoresPagoInit[ui.value];
            $("#pago_inicial").val(calPagoInit());
            $("#pago_mensual_fijo").val(pagoMensualFijo());
            $("#subtotal").val(subtotal());
            $("#iva").val(iva());
            $("#renta_en_deposito").val(rentaDeposito()); 
            $("#total_pago_inicial").val(totalPagoInicial()); 
            $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
            $("#total_por_pagar").val(totalPorPagar());
            }
        });
        $( "#valoresPagoInicial" ).text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")] + "%" );
        $( "#valorPagoInicialOculto").text(valoresPagoInit[$( "#sliderPagoInicial" ).slider( "value")]);
    });
}

// funcion para ejecutar el calculo al ingresar datos en el valor de factura sin iva
$("#valor_factura_sin_iva").keyup(function(event){       
    $("#pago_inicial").val(calPagoInit());
    $("#subtotal").val(subtotal());
    $("#iva").val(iva());
    $("#pago_mensual_fijo").val(pagoMensualFijo());
    $("#renta_en_deposito").val(rentaDeposito()); 
    $("#total_pago_inicial").val(totalPagoInicial()); 
    $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
    $("#total_por_pagar").val(totalPorPagar());
}); 

// funcion para pasarle el valor a factura sin iva
$("#valor_factura_con_iva").keyup(function(event){       
    var valor_factura_sin_iva = ($("#valor_factura_con_iva").val() / 1.16);
    $("#valor_factura_sin_iva").val(valor_factura_sin_iva.toFixed(2));
    $("#pago_inicial").val(calPagoInit());
    $("#subtotal").val(subtotal());
    $("#iva").val(iva());
    $("#pago_mensual_fijo").val(pagoMensualFijo());
    $("#renta_en_deposito").val(rentaDeposito()); 
    $("#total_pago_inicial").val(totalPagoInicial()); 
    $("#valor_residual_sin_iva").val(valorResidualSinIva()); 
    $("#total_por_pagar").val(totalPorPagar());
    
}); 

// función para calcular el pago inicial 
function calPagoInit(){
    var valorFactura =  $("#valor_factura_sin_iva").val();    
    var valorPagoInicial = $("#valorPagoInicialOculto").text();
    var vpi = (valorPagoInicial / 100);
    var result = (vpi * valorFactura);
    return result.toFixed(2);
}

// funcion para sacar el subtototal
function subtotal() {
    var valorFactura = $("#valor_factura_sin_iva").val();
    var valorPagoInicial = $("#valorPagoInicialOculto").text();   
    var pago_inicial = (valorPagoInicial / 100) * valorFactura;   
    var importe = valorFactura * .03;   
    var subtotal = importe + pago_inicial;
    return subtotal.toFixed(2);
}

// funcion para sacar el pago mensual fijo
function pagoMensualFijo() {
    var valorFactura = $("#valor_factura_sin_iva").val();
    var pago_inicial = ($("#valorPagoInicialOculto").text() /100);
    var importe_monto_financiar = (pago_inicial * valorFactura );
    var monto_financiar = valorFactura - importe_monto_financiar;
    var plazo = $("#valorPlazoOculto").text();

    var valor_residual = valorResidual();
    getValorResidual(valor_residual);
    var importe_valor_residual = ((valor_residual /100) * valorFactura);
    
    var pago = PMT((.3/12),plazo,(-(monto_financiar)),importe_valor_residual);
    
    var iva = (pago * .16);

    var result = pago + iva;

    return result.toFixed(2);
}

// funcion para sacar el valor residual
function getValorResidual(valor_residual) {
    $("#valor_residual_calculo").val(valor_residual);
}

// funcion replica de PMT de excel
function PMT(rate, nperiod, pv, fv, type) 
{ 
    if (!fv) fv = 0; if (!type) type = 0; if (rate == 0) return -(pv + fv)/nperiod; var pvif = Math.pow(1 + rate, nperiod); var pmt = rate / (pvif - 1) * -(pv * pvif + fv); if (type == 1) { pmt /= (1 + rate); }; return pmt; 
}

function iva(){
    var subtotal = $("#subtotal").val();

    var iva = (subtotal * .16);
    return iva.toFixed(2);
}

function rentaDeposito() {
    var pagoMensualFijo = $("#pago_mensual_fijo").val();
    var rentaDeposito = (pagoMensualFijo / 1.16);
    return rentaDeposito.toFixed(2);
}

function totalPagoInicial(){
    var subtotal = $("#subtotal").val();
    var iva = $("#iva").val();
    var rentaDeposito = $("#renta_en_deposito").val();
    
    var total = parseFloat(subtotal) + parseFloat(iva) + parseFloat(rentaDeposito);
    
    return total.toFixed(2);
}

function valorResidualSinIva() {
    var valor_residual = valorResidual();
    var valor_sin_iva = valor_residual + 10;
    var valorFactura = $("#valor_factura_sin_iva").val();
    var final = (valor_sin_iva/100) * valorFactura
    return final.toFixed(2);
}


function valorResidual() {
    var valor_residual = 0;
    var tipo_bien = $("#selectBienes").val();
    var plazo = $("#valorPlazoOculto").text();
    if(tipo_bien == "Particulares")
    {
        if(plazo == 12)
        {
            valor_residual = 50;
        }
        else if(plazo == 24)
        {
            valor_residual = 40;
        }
        else if(plazo == 36)
        {
            valor_residual = 30;
        }
        else if(plazo == 48)
        {
            valor_residual = 20;
        }
    }
    else if(tipo_bien == "Flotillas")
    {
        if(plazo == 12)
        {
            valor_residual = 40;
        }
        else if(plazo == 24)
        {
            valor_residual = 30;
        }
        else if(plazo == 36)
        {
            valor_residual = 10;
        }
        else if(plazo == 48)
        {
            valor_residual = 5;
        }
    }
    else if(tipo_bien == "Camiones")
    {
        if(plazo == 12)
        {
            valor_residual = 35;
        }
        else if(plazo == 24)
        {
            valor_residual = 25;
        }
        else if(plazo == 36)
        {
            valor_residual = 15;
        }
        else if(plazo == 48)
        {
            valor_residual = 5;
        }
    }
    else if(tipo_bien == "Maquinaria")
    {
        if(plazo == 12)
        {
            valor_residual = 40;
        }
        else if(plazo == 24)
        {
            valor_residual = 30;
        }
        else if(plazo == 36)
        {
            valor_residual = 10;
        }
        else if(plazo == 48)
        {
            valor_residual = 0;
        }
    }
    else{
        if(plazo == 12)
        {
            valor_residual = 20;
        }
        else if(plazo == 24)
        {
            valor_residual = 10;
        }
        else if(plazo == 36)
        {
            valor_residual = 0;
        }
        else if(plazo == 48)
        {
            valor_residual = 0;
        }
    }
    return valor_residual;
} 

function totalPorPagar(){
    var total_pago_inicial = $("#total_pago_inicial").val();
    var plazo = $( "#valorPlazoOculto" ).text();
    var pago_mensual_fijo = $("#pago_mensual_fijo").val(); 
    var valor_residual_sin_iva = $("#valor_residual_sin_iva").val();
    var total_a_pagar = ((parseFloat(total_pago_inicial) + (plazo - 1) * parseFloat(pago_mensual_fijo) + (parseFloat(pago_mensual_fijo) / 1.16 * .16) + parseFloat(valor_residual_sin_iva) * 1.16)); 
    
    return total_a_pagar.toFixed(2);
}
