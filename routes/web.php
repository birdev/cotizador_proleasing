<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('/cotizador');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('users', 'UserController');

Route::resource('biens', 'BienController');

Route::resource('tasas', 'TasaController');

Route::resource('valorTasas', 'ValorTasaController');

Route::resource('cotizacions', 'CotizacionController');

Route::get('pdf/{id}',['as' => 'crearPDF', 'uses' => 'CotizacionController@crearPDF']);

Route::get('verpdf/{id}',['as' => 'pdf', 'uses' => 'CotizacionController@pdf']);

Route::get('cotizaciones/web',['as' => 'cotizacionesWeb', 'uses' => 'CotizacionController@cotizacionesWeb']);

Route::get('/cotizador', 'FrontController@index');

Route::post('enviarCotizacion',['as' => 'front.enviar_cotizacion', 'uses' => 'FrontController@enviar_cotizacion']);

Route::get('/getValores/{id}',['as' => 'getValores', 'uses' => 'TasaController@getValores']);

Route::get('hola',['as' => 'front.info', 'uses' => 'FrontController@phpinfo']);
